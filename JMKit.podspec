#
#  Be sure to run `pod spec lint JMKit.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|


  s.name         = "JMKit"
    s.version      = "0.8.9"
    s.summary      = "UI Fast Use for iOS"
    s.description  = "快速创建文本，按钮，输入框，设置各种控件属性，快速完成UI"

    s.homepage     = "https://gitlab.com/FKFuture/jmkit"
    s.license              =  "MIT"

    s.author             = { "JM" => "asdfgwjm@163.com" }
    s.source       = { :git => "https://gitlab.com/FKFuture/jmkit.git", :tag => "0.8.9" }

  s.subspec 'JMExtension' do |mt|
    mt.source_files = 'Demo/JMKit/JMKit/Source/JMExtension/*.{h,m}'
    mt.public_header_files = 'Demo/JMKit/JMKit/Source/JMExtension/*.h'
    mt.dependency 'Masonry'

  end
  
    s.subspec 'JMKitHeader' do |mt|
    mt.source_files = 'Demo/JMKit/JMKit/Source/JMKitHeader/*.{h,m}'
    mt.public_header_files = 'Demo/JMKit/JMKit/Source/JMKitHeader/*.h'
    mt.dependency 'JMKit/JMExtension'
    mt.dependency 'JMKit/JMFactory'
    mt.dependency 'JMKit/JMAlert'

  end

  s.subspec 'JMFactory' do |mf|
    mf.source_files = 'Demo/JMKit/JMKit/Source/JMFactory/*.{h,m}'
    mf.public_header_files = 'Demo/JMKit/JMKit/Source/JMFactory/*.h'
    mf.dependency 'Masonry'
    mf.dependency 'JMKit/JMExtension'
  end

  s.subspec 'JMRefresh' do |mh|
    mh.source_files = 'Demo/JMKit/JMKit/Source/JMRefresh/*.{h,m}'
    mh.public_header_files = 'Demo/JMKit/JMKit/Source/JMRefresh/*.h'
    mh.dependency 'JMKit/JMExtension'
  end
  s.subspec 'JMAlert' do |mt|
    mt.source_files = 'Demo/JMKit/JMKit/Source/JMAlert/*.{h,m}'
    mt.public_header_files = 'Demo/JMKit/JMKit/Source/JMAlert/*.h'
    mt.dependency 'Masonry'
    mt.dependency 'JMKit/JMExtension'
    mt.dependency 'JMKit/JMFactory'

  end


    s.resource  =  'Demo/JMKit/JMKit/Source/Resource/*.png'


    s.platform     = :ios, "9.0"
    s.frameworks = "Foundation","UIKit"


  # s.public_header_files = "Classes/**/*.h"


  # s.resource  = "icon.png"
  # s.resources = "Resources/*.png"

  # s.preserve_paths = "FilesToSave", "MoreFilesToSave"


  # s.library   = "iconv"
  # s.libraries = "iconv", "xml2"



  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # s.dependency "JSONKit", "~> 1.4"

end
