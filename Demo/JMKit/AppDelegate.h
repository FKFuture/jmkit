//
//  AppDelegate.h
//  JMKit
//
//  Created by JM on 2019/7/9.
//  Copyright © 2019 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

