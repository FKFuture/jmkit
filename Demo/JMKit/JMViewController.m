//
//  JMViewController.m
//  JMKit
//
//  Created by JM on 2019/8/21.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "JMViewController.h"
#import "JMKitHeader.h"
#import <Masonry.h>
#import "JMFitButton.h"
#import "JMNumberTipView.h"

@interface JMViewController ()

@property(nonatomic,assign)NSInteger testIndex;


@end

@implementation JMViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    JMLabel *testLabel = [JMFactory createLabelWithTitle:@"测试下划线" Font:[UIFont systemFontOfSize:15] textColor:[UIColor blackColor]];
    testLabel.backgroundColor = [UIColor grayColor];
    [self.view addSubview:testLabel];
    [testLabel addUnderLineToText:@"测试" lineColor:[UIColor redColor] underlineStyle:NSUnderlineStyleDouble];
    [testLabel addBackGroundColorToText:@"下划线" bgColor:[UIColor blueColor]];
    
    [testLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.top.offset(100);
        make.size.offset(100);
        
    }];
    
    
    return;
    
    JMNumberTipView *tipView = [[JMNumberTipView alloc] init];
    
    [tipView changeNumberFont:[UIFont systemFontOfSize:25] textColor:[UIColor orangeColor] backGroundColor:[UIColor grayColor]];
    
    [tipView updateShowValue:5];

    
    [self.view addSubview:tipView];
    
    [tipView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.center.offset(0);
        
    }];
    

}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
        
    
}


@end
