//
//  JMNumberField.m
//  JMKitUpdate
//
//  Created by JM on 2020/2/12.
//  Copyright © 2020 YQ. All rights reserved.
//

#import "JMNumberField.h"

@interface JMNumberField ()<UITextFieldDelegate>

@end

@implementation JMNumberField

- (instancetype)initWithTextFontSize:(CGFloat)fontSize Placehoder:(NSString *)placeholder supportStyle:(NumberSupportStyle)style
{
    self = [super init];
    if (self) {
        
        self.font = [UIFont systemFontOfSize:fontSize];
        self.supportStyle = style;
        self.delegate = self;
        self.limitAfterPointCount = 9999;
        
        if (style == SupportNumber) {
            self.keyboardType = UIKeyboardTypeNumberPad;
        }else{
            self.keyboardType = UIKeyboardTypeDecimalPad;
        }
        
    }
    return self;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (self.supportStyle == SupportNumber) {
        
        return [self isPurnInt:string];
    }
    
    if (self.supportStyle == SupportFloat) {
        
        return [self isFloat:string];
    }

    
    return YES;
}

- (BOOL) isPurnInt:(NSString *)str{
    
    if ([str isEqualToString:@""]) {
        return YES;
    }
    
    if (str.length <= 0) {
        return NO;
    }
    
    NSString *patten = @"[0-9]{1,}";
    
    NSPredicate  *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",patten];
    
    if (![predicate evaluateWithObject:str]) {
        
        return NO;
    }

    
    return YES;
    
}

- (BOOL) isFloat:(NSString *)str{
    
    if ([str isEqualToString:@""]) {
        return YES;
    }
        
    if (self.text.length == 0 && [str isEqualToString:@"."]) {
        return NO;
    }
    
    if (![self.text containsString:@"."] && [str isEqualToString:@"."]) {
        return YES;
    }
    
    NSString *patten = @"^[0-9]+([.]{1}[0-9]+){0,1}$";
    
    NSPredicate  *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",patten];
    
    if (![predicate evaluateWithObject:str]) {
        
        return NO;
    }
    
    return YES;
    
}

- ( void)LimitString{
    
    if ([self.text isEqualToString:@""]) {
        return;
    }
    
    if ([self getNumberAfertPoint] > self.limitAfterPointCount) {
        
        self.text = [self.text substringToIndex:self.text.length - [self getNumberAfertPoint] + self.limitAfterPointCount ];
        
    }
    
    
    
}


- (NSInteger)getNumberAfertPoint{
    
    if ([self.text containsString:@"."]) {
        
        NSArray *array = [self.text componentsSeparatedByString:@"."];
        
        if (array.count == 2) {
            
            NSString *content = array[1];
            
            return content.length;
        }
        
        return 0;
        
    }
    
    return 0;
    
}







@end
