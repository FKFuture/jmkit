//
//  JMButton.m
//  JMKit
//
//  Created by JM on 2019/6/24.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "JMButton.h"
#import "UIControl+JMExtension.h"
#import "UIView+JMExtension.h"

@interface JMButton ()

@property (nonatomic,assign) CGFloat imageTitleMargin;//图片和文本间隔
@property(nonatomic,assign) CGFloat cornerValue;//圆角大小
@property(nonatomic,assign) UIRectCorner cornerType;//圆角类型
@property (nonatomic,assign)ImageTitleListMode imageTitleModel;//文本图片排列模式



@end

@implementation JMButton



- (CGRect)titleRectForContentRect:(CGRect)contentRect{


    if (!CGRectIsEmpty(self.titleRect) && !CGRectEqualToRect(self.titleRect, CGRectZero)) {
        return self.titleRect;
    }
    return [super titleRectForContentRect:contentRect];
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect{

    if (!CGRectIsEmpty(self.imageRect) && !CGRectEqualToRect(self.imageRect, CGRectZero)) {
        return self.imageRect;
    }
    return [super imageRectForContentRect:contentRect];
}

#pragma mark --设置按钮的圆角类型和圆角大小

- (void)setCornerValue:(CGFloat)cornerValue CornerType:(UIRectCorner)cornerType{
    
    self.cornerType = cornerType;
    self.cornerValue = cornerValue;
    
}



//在这里设置渐变色，即使按钮变大变小渐变色也一样
- (void)drawRect:(CGRect)rect{
    
    [super drawRect:rect];
    
    if (self.cornerType && self.cornerValue > 0) {
        
        //设置圆角
        CGRect boundRect = CGRectMake(0, 0,self.bounds.size.width ,self.bounds.size.height);

        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:self.cornerType cornerRadii:CGSizeMake(self.cornerValue, self.cornerValue)];

        CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];

        //设置大小
        maskLayer.frame = boundRect;
        maskLayer.path = maskPath.CGPath;

        self.layer.mask = maskLayer;

    }
    
    
    /*
     直接调用setNeedsDisplay或者setNeedsDisplayInRect:会触发drawRect：，但是有一个前提就是frame的size不能为0
     2、drawRect的调用时机是在viewWillAppear和viewDidAppear之间
     3、调用sizeToFit，会触发drawRect的调用
     4、UIView的contentMode属性设置成UIViewContentModeRedraw，每一次设置或更改frame值为触发drawRect的调用
     */


    /*
     layoutSubViews的调用时机
     1、setNeedsLayout或者layoutIfNeed
     2、addSubview
     3、改变一个view的frame值的时候，当然前提是frame值前后发生了变化
     4、屏幕旋转的时候会触发父视图的layoutSubviews
     5、scrollView在滑动的过程中触发UIView重新布局的时候会执行layoutSubviews
     */

}


- (void)layoutSubviews{

    [super layoutSubviews];
    
    CGSize buttonSize = self.bounds.size;
    CGRect imageRect = [self imageRectForContentRect:self.bounds];
    CGRect titleRect = [self titleRectForContentRect:self.bounds];
    
    CGFloat margin = self.imageTitleMargin;
    
    if (self.addTouchWidth > 0 && CGRectEqualToRect(self.enableTouchRect, CGRectZero)) {
        
        self.enableTouchRect = CGRectMake(-self.addTouchWidth,-self.addTouchWidth , buttonSize.width + self.addTouchWidth *2, buttonSize.height + self.addTouchWidth *2);
    }
    
    if (self.CustomImageTitleRect) {
        
        self.imageView.frame = self.imageRect;
        self.titleLabel.frame = self.titleRect;
        return;
    }

    //图片在上，文本在下
    if (self.imageTitleModel == ImageTopMode) {

        CGFloat totalHeight = imageRect.size.height + titleRect.size.height + margin;
        CGFloat y = buttonSize.height/2 - totalHeight/2;

        self.imageView.frame = CGRectMake(buttonSize.width/2 - imageRect.size.width/2, y, imageRect.size.width, imageRect.size.height);
        self.titleLabel.frame = CGRectMake(buttonSize.width/2 - titleRect.size.width/2,CGRectGetMaxY(self.imageView.frame) + margin, titleRect.size.width, titleRect.size.height);

    }

    if (self.imageTitleModel == ImageBottomMode) {

        CGFloat totalHeight = imageRect.size.height + titleRect.size.height + margin;
        CGFloat y = buttonSize.height/2 - totalHeight/2;

        self.titleLabel.frame = CGRectMake(buttonSize.width/2 - titleRect.size.width/2,y, titleRect.size.width, titleRect.size.height);
        self.imageView.frame = CGRectMake(buttonSize.width/2 - imageRect.size.width/2, CGRectGetMaxY(self.titleLabel.frame) + margin, imageRect.size.width, imageRect.size.height);

    }

    if (self.imageTitleModel == ImageLeftMode) {

        CGFloat totalWidth = imageRect.size.width + titleRect.size.width + margin;
        CGFloat x = buttonSize.width/2 - totalWidth/2;
        self.titleLabel.textAlignment = NSTextAlignmentLeft;

        self.imageView.frame = CGRectMake(x, buttonSize.height/2 - imageRect.size.height/2, imageRect.size.width, imageRect.size.height);
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.imageView.frame)+margin,buttonSize.height/2 - titleRect.size.height/2, titleRect.size.width, titleRect.size.height);

    }

    if (self.imageTitleModel == ImageRightMode) {

        CGFloat totalWidth = imageRect.size.width + titleRect.size.width + margin;
        CGFloat x = buttonSize.width/2 - totalWidth/2;

        self.titleLabel.frame = CGRectMake(x,buttonSize.height/2 - titleRect.size.height/2, titleRect.size.width, titleRect.size.height);
        self.titleLabel.textAlignment = NSTextAlignmentRight;
        self.imageView.frame = CGRectMake(CGRectGetMaxX(self.titleLabel.frame)+margin, buttonSize.height/2 - imageRect.size.height/2, imageRect.size.width, imageRect.size.height);

    }

}



- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event{

    //首先判断按钮有没有自定义按钮区域
    if (CGRectEqualToRect(CGRectZero, self.enableTouchRect) || CGRectIsEmpty(self.enableTouchRect)) {
        return [super pointInside:point withEvent:event];
    }

    if (CGRectContainsPoint(self.enableTouchRect,point)) {

        return YES;
    }

    return NO;

}


/// 设置图片文本模式
/// @param mode 图片文本模式
/// @param margin 图片和文本间隔
- (void)changeImageTitleModel:(ImageTitleListMode)mode WithMargin:(CGFloat)margin;
{

    self.imageTitleModel = mode;
    self.imageTitleMargin = margin;

}

- (void)changeCharacterWithKey:(NSString *)Key Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color State:(UIControlState)state iSFindAll:(BOOL)findAll{
    
    if (Key.length <= 0) return ;
    
    if (!font) {
        font = self.titleLabel.font;
    }
    if (!color){
        color = self.titleLabel.textColor;
    }
    
    if (!findAll) {
        
        NSMutableAttributedString *attributed = [self.titleLabel.attributedText mutableCopy];

        NSRange range = [self.currentTitle rangeOfString:Key];

        if (range.location != NSNotFound && font && color) {

            [attributed setAttributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:font} range:range];

            [self setAttributedTitle:attributed forState:state];

        }

        return;
    }
    
    
    NSMutableAttributedString *attributed = [self.titleLabel.attributedText mutableCopy];

    NSString *findString = self.currentTitle.copy;
    
    while ([findString rangeOfString:Key].location != NSNotFound) {
        
        NSRange range = [findString rangeOfString:Key];
        
        NSMutableString *replaceString = @"".mutableCopy;

        for (int i=0; i<range.length; i++) {
            
            [replaceString appendString:@" "];
        }
        
        findString = [findString stringByReplacingCharactersInRange:range withString:replaceString];
        

        [attributed setAttributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:font} range:range];

    }
    
    [self setAttributedTitle:attributed forState:state];

}


/// 改变文本中指定字符数组的颜色，字体
/// @param KeyArray 修改数组（key数组）
/// @param font 字体大小
/// @param color 字体颜色
/// @param findAll 是否修改所有的Key(一段文本中可能存在多个重复的Key)
-(void)changeCharacterWithKeyArray:(NSArray *)KeyArray Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color State:(UIControlState)state iSFindAll:(BOOL)findAll{

    for (int i=0; i<KeyArray.count; i++) {
        
        NSString *changeString = KeyArray[i];
        
        [self changeCharacterWithKey:changeString Font:font color:color State:state iSFindAll:findAll];
        
        
    }
    
}

/// 改变文本中指定范围的字体颜色大小
/// @param range 指定范围
/// @param font 字体大小
/// @param color 字体颜色
-(void)changeCharacterWithRang:(NSRange)range Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color State:(UIControlState)state{

    if (range.location == NSNotFound) return ;

    if (!font) {
        font = self.titleLabel.font;
    }
    if (!color){
        color = self.titleLabel.textColor;
    }
    
    NSMutableAttributedString *attributed = [self.titleLabel.attributedText mutableCopy];

    if (range.location != NSNotFound && font && color) {
        
        [attributed setAttributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:font} range:range];
    }
    
    [self setAttributedTitle:attributed forState:state];


}



@end
