//
//  JMControl.m
//  JMKitUpdate
//
//  Created by JM on 2022/2/15.
//  Copyright © 2022 YQ. All rights reserved.
//

#import "JMControl.h"

@implementation JMControl

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [self initAction];
    }
    return self;
}

- (void)initAction{
    
    [self addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    
    
}

- (void)click{
    
    if (self.ClickAction) {
        self.ClickAction();
    }
    
}

@end
