//
//  JMListView.m
//  JMKitUpdate
//
//  Created by JM on 2022/2/15.
//  Copyright © 2022 YQ. All rights reserved.
//

#import "JMListView.h"
#import "Masonry.h"
#import "NSArray+JMExtension.h"
#import "JMScrollView.h"

@interface JMListView ()

@property(nonatomic,strong)NSArray *dataArr;
@property(nonatomic,assign)CGFloat leftPadding;
@property(nonatomic,assign)CGFloat rightPadding;
@property(nonatomic,assign)CGFloat itemMargin;
@property(nonatomic,assign)CGFloat itemWidth;
@property(nonatomic,assign)CGFloat itemHeight;
@property(nonatomic,assign)NSInteger rowCount;//行数量
@property(nonatomic,assign)NSInteger colunmnCount;//列数量
@property(nonatomic,strong)JMScrollView *scrollView;//滑动视图

@property(nonatomic,strong)NSMutableArray *ContentViewArray;//所有子视图数组


@end

@implementation JMListView

- (instancetype)initWithActionList:(NSArray *)actionList RowCount:(NSInteger)rowCount columnCount:(NSInteger)column leftPadding:(CGFloat)leftPadding rightPadding:(CGFloat)rightPadding itemMargin:(CGFloat)itemMargin itemWidth:(CGFloat)itemWidth itemHeight:(CGFloat)itemHeight listDelegate:(id<JMListActionDelegate>)listDelegate;
{
    self = [super init];
    if (self) {
        
        self.dataArr = actionList;
        self.rowCount = rowCount;
        self.colunmnCount = column;
        self.leftPadding = leftPadding;
        self.rightPadding = rightPadding;
        self.itemMargin = itemMargin;
        self.itemWidth = itemWidth;
        self.itemHeight = itemHeight;
        self.listDelegate = listDelegate;
        
        self.ContentViewArray = [NSMutableArray arrayWithCapacity:0];
        
        [self createListViews];
        
    }
    return self;
}

- (void)createListViews{
    
    self.scrollView = [[JMScrollView alloc] init];
    self.scrollView.contentScrollStatus = JMHorizontalScrollStatus;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    [self addSubview:self.scrollView];
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.right.top.bottom.offset(0);
        
    }];
    
    NSInteger pageItemCount = self.rowCount * self.colunmnCount;
    
    if (pageItemCount <= 0) return;
    
    //分页数量
    NSInteger pageCount = self.dataArr.count/pageItemCount;
    
    NSInteger remainCount = self.dataArr.count % pageItemCount;
    
    if (remainCount > 0) {
        
        pageCount ++ ;
    }
        
    NSMutableArray *pageViewArray = [NSMutableArray arrayWithCapacity:0];
        
    CGFloat pageWidth = self.leftPadding + self.rightPadding + self.itemWidth*self.colunmnCount + (self.colunmnCount - 1)*self.itemMargin;
        
    CGFloat pageHeight = self.itemHeight*self.rowCount + (self.rowCount - 1)*self.itemMargin;

    for (int i=0; i<pageCount; i++) {
        
        UIView *pageView = [[UIView alloc] init];
        [self.scrollView addSubview:pageView];
        
        [pageViewArray addObject:pageView];
        
        [pageView mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.width.equalTo(self.mas_width).offset(0);
            make.width.offset(pageWidth);
            make.height.offset(pageHeight);
            
        }];
        
        NSMutableArray *itemViewsArray = [NSMutableArray arrayWithCapacity:0];
        
        NSInteger limitCount = pageItemCount;
        
        if (i == pageCount - 1) {
            
            limitCount = self.dataArr.count - (pageCount - 1)*pageItemCount;
            
        }

        for (int j=0; j<limitCount;j++) {
            
            NSInteger currentRow = j/self.colunmnCount ;
            NSInteger currentColumn = j % self.colunmnCount;
            
            NSInteger index = i*pageItemCount + j;

            if (self.listDelegate && [self.listDelegate respondsToSelector:@selector(getItemViewWithPage:row:column:index:)]) {
                
                UIView *view = [self.listDelegate getItemViewWithPage:i row:currentRow column:currentColumn index:index];
                
                [pageView addSubview:view];
                
                [itemViewsArray addObject:view];
                
                [self.ContentViewArray addObject:view];
                
                [view mas_makeConstraints:^(MASConstraintMaker *make) {
                    
                    make.left.offset(self.leftPadding + (self.itemWidth + self.itemMargin) * currentColumn);
                    make.top.offset((self.itemHeight +self.itemMargin)* currentRow);
                    make.width.offset(self.itemWidth);
                    make.height.offset(self.itemHeight);
                    
                }];
                            
            }

        }
        
        
    }
    
    [pageViewArray makeHorizontalDirectionWithFixSpacing:0 leadSpacing:0 tailSpacing:0 itemTopSpacing:0 itemBottomSpacing:0];


    
}

- (UIView *)getViewWithIndex:(NSInteger)index{
    
    UIView *view = self.ContentViewArray[index];
    
    return view;
}



@end
