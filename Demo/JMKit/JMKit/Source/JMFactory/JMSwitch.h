//
//  JMSwitch.h
//  JMKitUpdate
//
//  Created by JM on 2019/11/12.
//  Copyright © 2019 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JMSwitch : UISwitch

/// 设置开关的一些颜色
/// @param normalBackGroundColor 开关关闭状态下的背景颜色
/// @param openBackGroundColor 开关开启状态下的背景颜色
/// @param switchColor 开关按钮颜色
- (instancetype)initWithNormalBackGroundColor:(nullable UIColor *)normalBackGroundColor openBackGroundColor:(nullable UIColor *)openBackGroundColor switchColor:(nullable UIColor *)switchColor;

@end

NS_ASSUME_NONNULL_END
