//
//  JMListView.h
//  JMKitUpdate
//
//  Created by JM on 2022/2/15.
//  Copyright © 2022 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol JMListActionDelegate <NSObject>

/// 返回指定页指定列指定行的视图
/// @param page 当前页
/// @param row 当前行
/// @param column 当前列
/// @param index 当前索引
- (UIView *_Nonnull)getItemViewWithPage:(NSInteger)page row:(NSInteger)row column:(NSInteger)column index:(NSInteger)index;

@end

@class JMListAction;

NS_ASSUME_NONNULL_BEGIN

@interface JMListView : UIView

@property(nonatomic,weak)id<JMListActionDelegate> listDelegate;

/// 初始化按钮列表视图
/// @param actionList JMListAction数组
/// @param rowCount 每一页行数
/// @param column 每一页列数
/// @param leftPadding  每一页控件最左边和父视图间距
/// @param rightPadding 每一页控件最右边和父视图间距
/// @param itemMargin 每一个控件之间的水平，竖直方向间距
/// @param itemWidth 控件宽度
/// @param itemHeight 控件高度
- (instancetype)initWithActionList:(NSArray *)actionList RowCount:(NSInteger)rowCount columnCount:(NSInteger)column leftPadding:(CGFloat)leftPadding rightPadding:(CGFloat)rightPadding itemMargin:(CGFloat)itemMargin itemWidth:(CGFloat)itemWidth itemHeight:(CGFloat)itemHeight listDelegate:(id<JMListActionDelegate>)listDelegate;

/// 获取指定索引的视图
/// @param index 索引
- (UIView *)getViewWithIndex:(NSInteger)index;



@end

NS_ASSUME_NONNULL_END

@interface JMListAction : NSObject

/// 点击响应事件
@property(nonatomic, copy) void (^ _Nullable  ClickAction)(void);


@end


