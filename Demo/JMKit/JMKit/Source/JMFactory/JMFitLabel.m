//
//  JMFitLabel.m
//  JMKit
//
//  Created by JM on 2022/3/30.
//  Copyright © 2022 YQ. All rights reserved.
//

#import "JMFitLabel.h"

@interface JMFitLabel ()

@property(nonatomic,assign)UIEdgeInsets contentInset;


@end

@implementation JMFitLabel

- (instancetype)initContentInset:(UIEdgeInsets  )contentInset{
    
    self = [super initWithFrame:CGRectZero];
    
    if (self) {
        
        self.contentInset = contentInset;
        
    }
    
    return self;
    
}

- (CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines{
    
    CGRect newRect = [self getNewRectWithOriginRect:bounds inset:self.contentInset];
    

    CGRect rect = [super textRectForBounds:newRect limitedToNumberOfLines:numberOfLines];
    
    
    rect.origin.x -= self.contentInset.left; // 调整origin的X坐标
    rect.origin.y -= self.contentInset.top; // 调整origin的Y坐标
    rect.size.width += self.contentInset.left + self.contentInset.right ;// 调整size的width
    rect.size.height += self.contentInset.top + self.contentInset.bottom; // 调整size的height
    
    if (self.needAllCorner) {
        
        self.layer.cornerRadius = rect.size.height/2;
        self.layer.masksToBounds = YES;
    }
    
    return rect;

}

- (CGRect)getNewRectWithOriginRect:(CGRect)originRect inset:(UIEdgeInsets)inset{
    
    CGRect newRect = CGRectMake(originRect.origin.x + inset.left, originRect.origin.y + inset.top, originRect.size.width - inset.left - inset.right, originRect.size.height - inset.top - inset.bottom);
    
    return newRect;

}


- (void)drawTextInRect:(CGRect)rect{
        
    CGRect newRect = [self getNewRectWithOriginRect:rect inset:self.contentInset];
    
    
    [super drawTextInRect:newRect];
}

@end
