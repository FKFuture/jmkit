//
//  JMButton.h
//  JMKit
//
//  Created by JM on 2019/6/24.
//  Copyright © 2019 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum
{
    ImageLeftMode = 0,   //文本在右，图片在左
    ImageRightMode = 1,//文本在左，图片在右
    ImageTopMode = 2,//文本在下，图片在上
    ImageBottomMode = 3,//文本在上，图片在下

} ImageTitleListMode;

NS_ASSUME_NONNULL_BEGIN


@interface JMButton : UIButton

@property (nonatomic,assign) CGRect titleRect;//文本尺寸
@property (nonatomic,assign) CGRect imageRect;//图片尺寸
@property (nonatomic,assign) CGRect enableTouchRect; //按钮有效的触摸区域
@property (nonatomic,assign) CGFloat addTouchWidth; //添加按钮的触摸范围的整体尺寸
@property (nonatomic,assign) BOOL CustomImageTitleRect; //是否自定义图片文本尺寸

///设置按钮的圆角类型和圆角大小
/// @param cornerValue 圆角大小
/// @param cornerType 可设置只显示一个方向的圆角或者所有方向圆角
- (void)setCornerValue:(CGFloat)cornerValue CornerType:(UIRectCorner)cornerType;

//设置按钮/文本排列方式
- (void)changeImageTitleModel:(ImageTitleListMode)mode WithMargin:(CGFloat)margin;


@end

NS_ASSUME_NONNULL_END
