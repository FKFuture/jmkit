//
//  JMTextField.h
//  JMKit
//
//  Created by JM on 2022/2/24.
//  Copyright © 2022 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JMTextField : UITextField

//设置提示文字与输入框的顶部间距
@property(nonatomic,assign)CGFloat placeHolderMarginY;

//设置左右图片的大小会影响输入文本的范围，改变输入文本和提示文本的起点的结束点，决定光标的起始位置
//右侧图片占据的大小
@property(nonatomic,assign)CGFloat rightMargin;
//左侧图片占据的大小
@property(nonatomic,assign)CGFloat leftMargin;

//设置提示文字颜色
@property(nonatomic,strong)UIColor *placeholderColor;
//设置提示文字大小
@property(nonatomic,strong)UIFont *placeholderFont;

@property(nonatomic,assign)BOOL forbidCopy;//禁止复制

@property(nonatomic,assign)BOOL forbidSelete;//禁止选择

@property(nonatomic,assign)BOOL forbidAllSelete;//禁止全选

//设置最大字符长度
@property(nonatomic,assign)NSInteger maxStringLength;


@end

NS_ASSUME_NONNULL_END
