//
//  JMScrollView.h
//  JMKitUpdate
//
//  Created by JM on 2019/12/18.
//  Copyright © 2019 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

/** 刷新控件的状态 */
typedef NS_ENUM(NSInteger, JMScrollContetStatus) {
    /** 竖直方向滑动，内容宽度等于自身宽度，竖直方向高度自适应 */
    JMVerticalScrollStatus ,
    
    /**水平方向滑动，内容高度等于自身高度，水平方向宽度自适应 */
    JMHorizontalScrollStatus,
    
    /**水平，竖直方向都可滑动，内容宽度，高度自适应 */
    JMAllDirectionScrollStatus,

};

NS_ASSUME_NONNULL_BEGIN

/**
 自定义scrollView,根据设置设置滑动视图的内容宽度，高度
 */
@interface JMScrollView : UIScrollView


@property(nonatomic,assign)JMScrollContetStatus contentScrollStatus;


@end

NS_ASSUME_NONNULL_END
