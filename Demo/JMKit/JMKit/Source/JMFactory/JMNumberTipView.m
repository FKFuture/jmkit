//
//  JMNumberTipView.m
//  JMKit
//
//  Created by JM on 2022/4/27.
//  Copyright © 2022 YQ. All rights reserved.
//

#import "JMNumberTipView.h"
#import "Masonry.h"


@interface JMNumberTipView ()

@property(nonatomic,strong)UILabel *numberLabel;

@end

@implementation JMNumberTipView

- (instancetype)init{
    
    self = [super init];
    
    if (self) {
        [self creatSubviews];
        [self addContrains];
    }
    
    return self;
}

- (void)creatSubviews{
    
    self.backgroundColor = [UIColor redColor];
    
    self.numberLabel = [[UILabel alloc] init];
    self.numberLabel.textColor = [UIColor whiteColor];
    self.numberLabel.font = [UIFont systemFontOfSize:13];
    self.numberLabel.textAlignment = NSTextAlignmentCenter;
    
    [self addSubview:self.numberLabel];
    
}

- (void)updateShowValue:(NSInteger)count{
    
    self.numberLabel.text = [NSString stringWithFormat:@"%ld",count];
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = (self.numberLabel.font.lineHeight + 6)/2;
    
    if (count <= 0) {
        self.hidden = YES;
        return;
    }
    
    self.hidden = NO;
    
    __weak UILabel *weakLabel = self.numberLabel;
    

    
    if (count < 10) {
        
        [self.numberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {

            make.width.height.offset(weakLabel.font.lineHeight + 6);
            make.left.right.top.bottom.offset(0);


        }];
        
    }else if(count < 99){
        
        [self.numberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {

            make.height.offset(weakLabel.font.lineHeight + 6);
            make.left.offset(10);
            make.right.offset(-10);
            make.top.bottom.offset(0);

        }];

    }else{
        
        self.numberLabel.text = @"99+";
        
        [self.numberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {

            make.height.offset(weakLabel.font.lineHeight + 6);
            make.left.offset(8);
            make.right.offset(-8);
            make.top.bottom.offset(0);

        }];

        
    }
    
    
}

- (void)changeNumberFont:(UIFont *)font textColor:(UIColor *)color backGroundColor:(UIColor *)backGroundColor{
    
    self.numberLabel.font = font;
    self.numberLabel.textColor = color;
    self.backgroundColor = backGroundColor;
}

- (void)addContrains{
    
    __weak UILabel *weakLabel = self.numberLabel;

    
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.height.offset(weakLabel.font.lineHeight + 6);
        make.left.right.top.bottom.offset(0);
        
    }];
    
}



@end
