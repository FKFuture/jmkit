//
//  JMCountDownButton.h
//  JMKitUpdate
//
//  Created by JM on 2022/2/14.
//  Copyright © 2022 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/// 倒计时按钮
@interface JMCountDownButton : UIButton

/// 初始化倒计时按钮（倒计时内按钮不可点击）
/// @param countTime 倒计时时长
/// @param startTitle 未点击之前显示的文本
/// @param endTitle 倒计时结束显示的文本

- (instancetype)initWithStartTitle:(NSString *)startTitle endTitle:(nullable NSString *)endTitle countTime:(CGFloat)countTime;

/// 开始倒计时
- (void)beginTimer;

/// 结束倒计时
- (void)endTimer;
    

@end

NS_ASSUME_NONNULL_END
