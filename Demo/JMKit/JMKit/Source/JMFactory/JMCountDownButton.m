//
//  JMCountDownButton.m
//  JMKitUpdate
//
//  Created by JM on 2022/2/14.
//  Copyright © 2022 YQ. All rights reserved.
//

#import "JMCountDownButton.h"

@interface JMCountDownButton ()

@property(nonatomic,strong)NSTimer *timer;
@property(nonatomic,assign)NSInteger time;
@property(nonatomic,copy)NSString * startTitle;
@property(nonatomic,copy)NSString * endTitle;

@end

@implementation JMCountDownButton

- (instancetype)initWithStartTitle:(NSString *)startTitle endTitle:(nullable NSString *)endTitle countTime:(CGFloat)countTime{

    self = [super init];

    if (self) {
        
        self.time = countTime;
        self.startTitle = startTitle;
        self.endTitle = endTitle;
        [self setTitle:self.startTitle forState:UIControlStateNormal];

    }
    
    return self;

}

- (void)setupTimer
{
    [self invalidateTimer]; // 创建定时器前先停止定时器，不然会出现僵尸定时器，导致轮播频率错误
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
    _timer = timer;
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

- (void)beginTimer {
    
    _time = self.time;
    [self setupTimer];
    self.userInteractionEnabled = NO;

}

- (void)endTimer {
    
    self.userInteractionEnabled = YES;
    [self invalidateTimer];
    
    [self setTitle:self.endTitle.length > 0 ? self.endTitle:self.startTitle forState:UIControlStateNormal];
}


- (void)timerAction {
    
    _time --;
    [self setTitle:[NSString stringWithFormat:@"%lds",(long)_time] forState:UIControlStateNormal];
    if (_time == 0) {
        
        [self endTimer];
        
    }
}


- (void)invalidateTimer
{
    if (_timer == nil) {
        return;
    }
    [_timer invalidate];
    _timer = nil;
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    if (!newSuperview) {
        [self invalidateTimer];
    }
}





@end
