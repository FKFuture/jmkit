//
//  JMScrollView.m
//  JMKitUpdate
//
//  Created by JM on 2019/12/18.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "JMScrollView.h"

@interface JMScrollView ()

@property(nonatomic,strong)NSLayoutConstraint *WidthLayoutContraint;
@property(nonatomic,strong)NSLayoutConstraint *HeightLayoutContraint;
@property (nonatomic,strong) UIView *contenView;


@end

@implementation JMScrollView


- (instancetype)init{

    self = [super init];

    if (self) {

        [self creatContentView];

    }

    return self;
}


- (void)creatContentView{

    self.contenView = [[UIView alloc] init];
    self.contenView.translatesAutoresizingMaskIntoConstraints = NO;
    if (@available(iOS 11.0, *)) {
        self.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }

    [self addSubview:self.contenView];

    NSLayoutConstraint *leftLayoutContraint = [NSLayoutConstraint constraintWithItem:self.contenView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:0];
    NSLayoutConstraint *rightLayoutContraint = [NSLayoutConstraint constraintWithItem:self.contenView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:0];
    NSLayoutConstraint *topLayoutContraint = [NSLayoutConstraint constraintWithItem:self.contenView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    NSLayoutConstraint *BottomLayoutContraint = [NSLayoutConstraint constraintWithItem:self.contenView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    
    self.WidthLayoutContraint = [NSLayoutConstraint constraintWithItem:self.contenView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1 constant:0];
    
    self.HeightLayoutContraint = [NSLayoutConstraint constraintWithItem:self.contenView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:1 constant:0];
        
    [self addConstraint:leftLayoutContraint];
    [self addConstraint:rightLayoutContraint];
    [self addConstraint:topLayoutContraint];
    [self addConstraint:BottomLayoutContraint];
    
    [self addConstraint:self.WidthLayoutContraint];
    [self addConstraint:self.HeightLayoutContraint];
    
    //默认是竖直方向滑动
    self.contentScrollStatus = JMVerticalScrollStatus;

    
}

- (void)setContentScrollStatus:(JMScrollContetStatus)contentScrollStatus{
    
    if (_contentScrollStatus != contentScrollStatus) {
        _contentScrollStatus = contentScrollStatus;
    }
    
    [self changeContraint];
}


- (void)changeContraint{
    
    [self removeConstraint:self.WidthLayoutContraint];
    [self removeConstraint:self.HeightLayoutContraint];

    if (self.contentScrollStatus == JMVerticalScrollStatus) {
        
        [self addConstraint:self.WidthLayoutContraint];
    }
    
    if (self.contentScrollStatus == JMHorizontalScrollStatus) {
        
        [self addConstraint:self.HeightLayoutContraint];
    }

    
}


@end
