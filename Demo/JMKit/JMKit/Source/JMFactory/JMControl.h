//
//  JMControl.h
//  JMKitUpdate
//
//  Created by JM on 2022/2/15.
//  Copyright © 2022 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JMControl : UIControl

@property(nonatomic, copy) void (^ClickAction)(void);


@end

NS_ASSUME_NONNULL_END
