//
//  JMTextField.m
//  JMKit
//
//  Created by JM on 2022/2/24.
//  Copyright © 2022 YQ. All rights reserved.
//

#import "JMTextField.h"

@implementation JMTextField

- (instancetype)init{
    
    self = [super init];
    
    if (self) {

        [self initData];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self initData];
    }
    
    return self;
    
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        [self initData];
    }
    
    return self;
}

- (void)initData{
    
    [self addTarget:self action:@selector(LimitString) forControlEvents:UIControlEventEditingChanged];

}

//设置提示文字颜色
- (void)setPlaceholderColor:(UIColor *)placeholderColor{
    
    if (self.placeholder.length <= 0) {
        return;
    }
    
    if (_placeholderColor!= placeholderColor) {
        
        _placeholderColor = placeholderColor;
        
        NSMutableAttributedString *attributedString = self.attributedPlaceholder.mutableCopy;
        
        [attributedString addAttributes:@{NSForegroundColorAttributeName:placeholderColor} range:NSMakeRange(0, self.placeholder.length)];
        
        self.attributedPlaceholder = attributedString;
        
    }


}

//设置提示文字字体大小
- (void)setPlaceholderFont:(UIFont *)placeholderFont{
    
    if (self.placeholder.length <= 0) {
        return;
    }
    
    if (_placeholderFont!= placeholderFont) {
        
        _placeholderFont = placeholderFont;

        //设置提示文字垂直居中
        NSMutableParagraphStyle *style = [self.defaultTextAttributes[NSParagraphStyleAttributeName] mutableCopy];

        style.minimumLineHeight = self.font.lineHeight - (self.font.lineHeight - placeholderFont.lineHeight) / 2.0;

        if (self.attributedPlaceholder) {

            
            NSMutableAttributedString *attributedString = self.attributedPlaceholder.mutableCopy;
            
            [attributedString addAttributes:@{NSFontAttributeName:placeholderFont,NSParagraphStyleAttributeName : style} range:NSMakeRange(0, self.placeholder.length)];
            
            self.attributedPlaceholder = attributedString;


        }

    }
}



//设置提示文字区域（坐标系是以输入框为原点）
- (CGRect)placeholderRectForBounds:(CGRect)bounds{

    return CGRectMake(bounds.origin.x+self.leftMargin, bounds.origin.y+self.placeHolderMarginY, bounds.size.width-self.leftMargin-self.rightMargin, bounds.size.height - self.placeHolderMarginY);
}


//设置编辑文本光标的位置（点击输入框就会调用,光标位置确定了，输入框的文本显示区域初始位置也就确定了）
- (CGRect)editingRectForBounds:(CGRect)bounds{


    CGRect frame = (CGRectMake(bounds.origin.x +self.leftMargin, bounds.origin.y + self.placeHolderMarginY, bounds.size.width-self.rightMargin - self.leftMargin , bounds.size.height - self.placeHolderMarginY));


    return frame;
}

//设置文本显示的区域
- (CGRect)textRectForBounds:(CGRect)bounds{

    return [self editingRectForBounds:bounds];

}


//根据需要禁止复制，选择，全选功能
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender{

    if (action == @selector(paste:)) {
        if (self.forbidCopy) return NO;
    }

    if (action == @selector(select:)) {
        if (self.forbidSelete) return NO;
    }

    if (action == @selector(selectAll:)) {
        if (self.forbidAllSelete) return NO;
    }

    return [super canPerformAction:action withSender:sender];

}


//限制输入框的输入字符长度
- ( void)LimitString{
    
    if (self.maxStringLength <= 0) return ;
    
    NSString *toBeString = self.text;
    
    if ([self.textInputMode.primaryLanguage isEqualToString:@"zh-Hans"]) { // 简体中文输入，包括简体拼音，健体五笔，简体手写
        
        UITextRange *selectedRange = [self markedTextRange];
        
        //获取高亮部分
        UITextPosition *position = [self positionFromPosition:selectedRange.start offset:0];
        
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            
            if (toBeString.length > self.maxStringLength) {
                
                self.text = [toBeString substringToIndex:self.maxStringLength];
                
            }
            
            //有高亮的文字则暂时不限制
        }else{
            
            
        }
    }else{
        
        if (toBeString.length > self.maxStringLength)
        {
            
            self.text = [self.text  substringWithRange:NSMakeRange(0, self.maxStringLength)];
        }
    }
    
}


@end
