//
//  JMNumberTipView.h
//  JMKit
//
//  Created by JM on 2022/4/27.
//  Copyright © 2022 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/// 消息数量显示视图
@interface JMNumberTipView : UIView

/// 更新显示数量
/// @param count 数量
- (void)updateShowValue:(NSInteger)count;

/// 设置数字的字体大小，颜色，背景颜色
/// @param font 字体大小
/// @param color 字体颜色
/// @param backGroundColor 背景颜色
- (void)changeNumberFont:(UIFont *)font textColor:(UIColor *)color backGroundColor:(UIColor *)backGroundColor;

@end

NS_ASSUME_NONNULL_END
