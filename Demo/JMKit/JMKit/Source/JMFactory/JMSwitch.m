//
//  JMSwitch.m
//  JMKitUpdate
//
//  Created by JM on 2019/11/12.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "JMSwitch.h"

@interface JMSwitch ()

@property(nonatomic,strong)UIColor *normalBackGroundColor;


@end

@implementation JMSwitch

- (instancetype)initWithNormalBackGroundColor:(nullable UIColor *)normalBackGroundColor openBackGroundColor:(nullable UIColor *)openBackGroundColor switchColor:(nullable UIColor *)switchColor{
    
    self = [super init];
    
    if (self) {
        
        //设置开关未选中背景颜色
        if (normalBackGroundColor != nil) {
    
            self.backgroundColor = normalBackGroundColor;
            self.normalBackGroundColor = normalBackGroundColor;

        }
        
        //设置选中背景颜色
        if (openBackGroundColor != nil) {
    
            self.onTintColor = openBackGroundColor;

        }
        
        //设置开关按键颜色（选中和未选中颜色）
        if (switchColor != nil) {
    
            self.thumbTintColor = switchColor;

        }



    }
    
    return self;
        
    
}

- (void)layoutSubviews{
    
    [super layoutSubviews];
    
    if (self.normalBackGroundColor != nil) {
        
        self.layer.cornerRadius = self.frame.size.height/2;
        self.layer.masksToBounds = YES;

    }

    
}










@end
