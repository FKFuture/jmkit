//
//  JMFactory.h
//  JMKit
//
//  Created by JM on 2019/6/24.
//  Copyright © 2019 YQ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class JMLabel;
@class JMButton;
@class JMTextField;
@class JMTextView;



NS_ASSUME_NONNULL_BEGIN

/**
 控件工厂类
 */
@interface JMFactory : NSObject

#pragma mark -- 文本类

/// 创建文本控件
/// @param font 字体
+(JMLabel *)createLabelWithFont:(UIFont *)font;

/// 常见文本控件
/// @param font 字体
/// @param textColor 字体颜色
+(JMLabel *)createLabelWithFont:(UIFont *)font textColor:(UIColor *)textColor;

/// 创建文本控件
/// @param title 文本内容
/// @param font 字体
/// @param textColor 字体颜色
+(JMLabel *)createLabelWithTitle:(NSString *)title Font:(UIFont *)font textColor:(UIColor *)textColor;

/// 创建文本控件
/// @param title 文本内容
/// @param font 字体
/// @param textColor 字体颜色
/// @param alignment 文本排列方式
+(JMLabel *)createLabelWithTitle:(NSString *)title Font:(UIFont *)font textColor:(UIColor *)textColor textAligment:(NSTextAlignment)alignment;

/// 创建文本控件
/// @param title 文本内容
/// @param font 字体
/// @param textColor 字体颜色
/// @param alignment 文本排列方式
/// @param lines 行数量，0代表自动换行
+(JMLabel *)createLabelWithTitle:(NSString *)title Font:(UIFont *)font textColor:(UIColor *)textColor textAligment:(NSTextAlignment)alignment numberLines:(NSInteger)lines ;



#pragma mark -- 创建按钮类

/// 创建按钮控件
/// @param title 按钮内容
/// @param titleColor 字体颜色
/// @param font 字体
/// @param target 响应类
/// @param selector 响应方法
+(JMButton *)createButtonWithTitle:(nullable NSString *)title TitleColor:(nullable UIColor *)titleColor Font:(nullable UIFont *)font  target:(nullable id)target selector:(nullable SEL)selector;


/// 创建按钮控件
/// @param imageName 背景图片名
/// @param target 响应类
/// @param selector 相应方法
+(JMButton *)createButtonWithBackGroundName:(nullable NSString *)imageName  target:(nullable id)target selector:(nullable SEL)selector;


/// 创建按钮控件
/// @param norImageName 正常状态下图片名
/// @param preImageName 选中状态下图片名
/// @param target 相应类
/// @param selector 响应方法
+(JMButton *)createButtonWithNorImageName:(nullable NSString *)norImageName  PreImageName:(nullable NSString *)preImageName target:(nullable id)target selector:(nullable SEL)selector;

  

/// 创建按钮控件
/// @param title 按钮文本
/// @param titleColor 文本颜色
/// @param font 文本字体
/// @param norImageName 正常状态下图片名
/// @param preImageName 选中状态下图片名
/// @param target 响应类
/// @param selector 响应方法
+(JMButton *)createButtonWithTitle:(nullable NSString *)title TitleColor:(nullable UIColor *)titleColor Font:(nullable UIFont *)font NorImageName:(nullable NSString *)norImageName PreImageName:(nullable NSString *)preImageName target:(nullable id)target selector:(nullable SEL)selector;



#pragma mark -- 创建输入框

/// 创建输入框
/// @param title 内容
/// @param textFont 字体
/// @param placeholder 提示文本
+(JMTextField *)createFieldWithTitle:(nullable NSString *)title TextFont:(UIFont *)textFont Placeholder:(nullable NSString *)placeholder;


/// 创建输入框
/// @param textColor 字体颜色
/// @param textFont 字体
/// @param placeholder 提示文本
/// @param placeholderColor 提示文本颜色
/// @param placeholderFont 提示文本字体
+(JMTextField *)createFieldWithTextColor:(nullable UIColor *)textColor TextFont:(UIFont *)textFont Placeholder:(nullable NSString *)placeholder PlaceholderColor:(nullable UIColor *)placeholderColor PlaceholderFont:(nullable UIFont *)placeholderFont;


#pragma mark -- 创建textView

+(JMTextView *)createTextViewWithFrame:(CGRect)rect andTextViewFont:(CGFloat)fontSize;



@end

NS_ASSUME_NONNULL_END
