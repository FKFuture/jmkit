//
//  JMNumberField.h
//  JMKitUpdate
//
//  Created by JM on 2020/2/12.
//  Copyright © 2020 YQ. All rights reserved.
//

#import "JMTextField.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    SupportNumber,   //只支持正数（无法输入.和其他符号）
    SupportFloat,    //支持浮点数
} NumberSupportStyle;

@interface JMNumberField : JMTextField

@property(nonatomic,assign)NumberSupportStyle supportStyle;
@property (nonatomic,assign)NSInteger  limitAfterPointCount;  //设置小数点后的位数

- (instancetype)initWithTextFontSize:(CGFloat)fontSize Placehoder:(NSString *)placeholder supportStyle:(NumberSupportStyle)style;


@end

NS_ASSUME_NONNULL_END
