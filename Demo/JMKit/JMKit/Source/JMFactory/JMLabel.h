//
//  JMLabel.h
//  JMKit
//
//  Created by JM on 2019/6/24.
//  Copyright © 2019 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum
{
    VerticalAlignmentMiddle = 0,//设置文本居中
    VerticalAlignmentTop = 1,   //设置文本从顶部开始
    VerticalAlignmentBottom = 2,//设置文本紧贴底部
    
} VerticalAlignment;

@interface JMLabel : UILabel

@property (nonatomic,assign) VerticalAlignment verticalAlignment;


@end

NS_ASSUME_NONNULL_END
