//
//  JMTextView.m
//  JMKit
//
//  Created by JM on 2019/6/24.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "JMTextView.h"
#import "UIColor+JMExtension.h"
#import "UIView+JMExtension.h"
#import "Masonry.h"


@interface JMTextView ()<UITextViewDelegate>

@property(nonatomic,strong)UILabel *MaxLengthTipLabel;//剩余文字变化文本
@property(nonatomic,copy)NSString *placeHolder;
@property(nonatomic,strong)UIFont *placeTextFont;
@property(nonatomic,strong)UIColor *placeTextColor;




@end


@implementation JMTextView

- (instancetype)initWithFrame:(CGRect)frame placeHolderString:(NSString *)placeHolderString placeHoderColor:(UIColor *)color placHoderFont:(UIFont *)font{

    self = [super initWithFrame:frame];

    if (self) {
        self.placeHolder = placeHolderString;
        self.placeTextFont = font;
        self.placeTextColor = color;
        [self initData];
    }

    return self;
}


- (void)initData{

    self.delegate = self;
    self.textContainerInset = UIEdgeInsetsMake(16, 17, 20, 17);

    [self creatSubviews];


}

- (void)creatSubviews{

    self.placeHolderLabel = [[JMLabel alloc] init];
    self.placeHolderLabel.textColor = self.placeTextColor;
    self.placeHolderLabel.text = self.placeHolder;
    self.placeHolderLabel.font = self.placeTextFont;
    self.placeHolderLabel.frame = CGRectMake(17, 16, 150, 14);

    self.MaxLengthTipLabel = [[UILabel alloc] init];
    self.MaxLengthTipLabel.textAlignment = NSTextAlignmentRight;
    self.MaxLengthTipLabel.textColor = self.placeTextColor;
    self.MaxLengthTipLabel.font = self.placeTextFont;
    self.MaxLengthTipLabel.frame = CGRectMake(10, self.jm_height - 25, self.jm_width-20, 15);
    self.MaxLengthTipLabel.text = [NSString stringWithFormat:@"0/%ld",self.maxTextCount];

    [self addSubview:self.placeHolderLabel];
    [self addSubview:self.MaxLengthTipLabel];

}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        return NO;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{

    if (textView.text.length > 0) {
        
         self.placeHolderLabel.hidden = YES;    
        
    }else{
        self.placeHolderLabel.hidden = NO;
    }
    

    if (self.maxTextCount > 0) {
        [self LimitStringWithMaxLength:self.maxTextCount];
    }

    //根据当前文字，提示剩余文字
//    self.MaxLengthTipLabel.text = [NSString stringWithFormat:@"%ld/%ld",textView.text.length,self.maxTextCount];

}

//监听到文本复制
- (void)textViewDidChangeSelection:(UITextView *)textView{

    self.placeHolderLabel.hidden = YES;

    if (textView.text.length > self.maxTextCount && self.maxTextCount > 0) {

        [self LimitStringWithMaxLength:self.maxTextCount];
    }

}





//给某个字符添加链接样式
- (void)addLinkWithString:(NSString *)string{

    if (!string) return ;

    NSMutableAttributedString *attributed = [self.attributedText mutableCopy];

    NSRange range = [self.text rangeOfString:string];

    if (range.location != NSNotFound) {
        [attributed addAttribute:NSLinkAttributeName value:string range:range];
    }

    self.attributedText = attributed;

}


/// 修改文本中指定字符串的字体大小/颜色（若文本中存在多个此文本会全部修改）
/// @param Key 指定文本
/// @param font 修改的字体大小
/// @param color 修改的字体颜色
- (void)changeCharacterWithRepeatKey:(NSString *)Key Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color{
        
    if (Key.length <= 0) return ;
    
    if (!font) {
        font = self.font;
    }
    if (!color){
        color = self.textColor;
    }
    
    NSMutableAttributedString *attributed = [self.attributedText mutableCopy];
        
    NSString *findString = self.text.copy;
    
    while ([findString rangeOfString:Key].location != NSNotFound) {
        
        NSRange range = [findString rangeOfString:Key];
        
        NSMutableString *replaceString = @"".mutableCopy;

        for (int i=0; i<range.length; i++) {
            
            [replaceString appendString:@" "];
        }
        
        findString = [findString stringByReplacingCharactersInRange:range withString:replaceString];
        

        [attributed setAttributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:font} range:range];

    }
    
    self.attributedText = attributed;

}

/// 改变文本中指定字符的颜色，字体(只限于查找到的第一个)
/// @param string 指定文本
/// @param font 修改的字体大小
/// @param color 修改的字体颜色
- (void)changeCharacterWithString:(NSString *)string  Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color{
    
    if (string.length <= 0) return ;
    
    if (!font) {
        font = self.font;
    }
    if (!color){
        color = self.textColor;
    }
    
    NSMutableAttributedString *attributed = [self.attributedText mutableCopy];
    
    NSRange range = [self.text rangeOfString:string];

    if (range.location != NSNotFound && font && color) {

        [attributed setAttributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:font} range:range];

        self.attributedText = attributed;

    }
    
}

/// 改变文本中指定字符数组的颜色，字体（数组中的文本会全部查找出来修改）
/// @param KeyArray 修改数组（key数组）
/// @param font 字体大小
/// @param color 字体颜色
-(void)changeCharacterWithRepeatKeyArray:(NSArray *)KeyArray Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color{

    for (int i=0; i<KeyArray.count; i++) {
        
        NSString *changeString = KeyArray[i];
        
        [self changeCharacterWithRepeatKey:changeString Font:font color:color];
                
    }
    
}

/// 改变文本中指定字符数组的颜色，字体（数组中的文本只会查找出一次修改）
/// @param KeyArray 修改数组（key数组）
/// @param font 字体大小
/// @param color 字体颜色
-(void)changeCharacterWithArray:(NSArray *)KeyArray Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color{

    for (int i=0; i<KeyArray.count; i++) {
        
        NSString *changeString = KeyArray[i];
        
        [self changeCharacterWithString:changeString Font:font color:color];
                
    }
    
}


/// 改变文本中指定范围的字体颜色大小
/// @param range 指定范围
/// @param font 字体大小
/// @param color 字体颜色
-(void)changeCharacterWithRang:(NSRange)range Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color {

    if (range.location == NSNotFound) return ;

    if (!font) {
        font = self.font;
    }

    if (!color){
        color = self.textColor;
    }
    NSMutableAttributedString *attributed = [self.attributedText mutableCopy];

        if (range.location != NSNotFound && font && color) {
            [attributed setAttributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:font} range:range];
        }


    self.attributedText = attributed;
}

//根据需要禁止复制，选择，全选功能
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender{

    if (action == @selector(paste:)) {
        if (self.forbidCopy) return NO;
    }

    if (action == @selector(select:)) {
        if (self.forbidSelete) return NO;
    }

    if (action == @selector(selectAll:)) {
        if (self.forbidAllSelete) return NO;
    }

    return [super canPerformAction:action withSender:sender];

}

- ( void)LimitStringWithMaxLength:(NSInteger )MaxLength{

    NSString *toBeString = self.text;

    if ([self.textInputMode.primaryLanguage isEqualToString:@"zh-Hans"]) { // 简体中文输入，包括简体拼音，健体五笔，简体手写

        UITextRange *selectedRange = [self markedTextRange];
        //获取高亮部分
        UITextPosition *position = [self positionFromPosition:selectedRange.start offset:0];

        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {

            if (toBeString.length > MaxLength) {

                self.text = [toBeString substringToIndex:MaxLength];
            }

                self.MaxLengthTipLabel.text = [NSString stringWithFormat:@"%ld/%ld",self.text.length,self.maxTextCount];


        }else{

            UITextPosition* beginning = self.beginningOfDocument;

            UITextPosition* selectionStart = selectedRange.start;
            NSInteger location = [self offsetFromPosition:beginning toPosition:selectionStart];


            self.MaxLengthTipLabel.text = [NSString stringWithFormat:@"%ld/%ld",location,self.maxTextCount];



        }
    }else{

        if (self.text.length > MaxLength)
        {

            self.text = [self.text  substringWithRange:NSMakeRange(0, MaxLength)];
        }

            self.MaxLengthTipLabel.text = [NSString stringWithFormat:@"%ld/%ld",self.text.length,self.maxTextCount];

    }

    if (self.contentSize.height > self.jm_height) {

        //获取当前contentSize
        self.MaxLengthTipLabel.frame = CGRectMake(10, self.contentSize.height - 20, self.jm_width-20, 15);

    }else{

        self.MaxLengthTipLabel.frame = CGRectMake(10, self.jm_height - 25, self.jm_width-20, 15);

    }


}

- (void)setMaxTextCount:(NSInteger)maxTextCount {
    
    _maxTextCount = maxTextCount;
    self.MaxLengthTipLabel.text = [NSString stringWithFormat:@"0/%ld",maxTextCount];
}

- (void)setIsHideMaxLabel:(BOOL)isHideMaxLabel{

    if (_isHideMaxLabel!= isHideMaxLabel) {
        _isHideMaxLabel = isHideMaxLabel;
        self.MaxLengthTipLabel.hidden = isHideMaxLabel;
    }
}

- (void)setIsHidePlaceLabel:(BOOL)isHidePlaceLabel{

    if (_isHidePlaceLabel!= isHidePlaceLabel) {
        _isHidePlaceLabel = isHidePlaceLabel;
        self.placeHolderLabel.hidden = isHidePlaceLabel;
    }
}



@end
