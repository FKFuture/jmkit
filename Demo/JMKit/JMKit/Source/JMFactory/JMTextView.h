//
//  JMTextView.h
//  JMKit
//
//  Created by JM on 2019/6/24.
//  Copyright © 2019 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JMLabel.h"
NS_ASSUME_NONNULL_BEGIN

@interface JMTextView : UITextView

@property(nonatomic,strong)JMLabel *placeHolderLabel;//提示文本

@property(nonatomic,assign)BOOL forbidCopy;
@property(nonatomic,assign)BOOL forbidSelete;
@property(nonatomic,assign)BOOL forbidAllSelete;
@property(nonatomic,assign)NSInteger maxTextCount;
@property(nonatomic,assign)BOOL isHidePlaceLabel; 
@property(nonatomic,assign)BOOL isHideMaxLabel;


/// 改变文本中指定字符的颜色，字体(只限于查找到的第一个)
/// @param string 指定文本
/// @param font 修改的字体大小
/// @param color 修改的字体颜色
- (void)changeCharacterWithString:(NSString *)string  Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color;

/// 修改文本中指定字符串的字体大小/颜色（若文本中存在多个此文本会全部修改）
/// @param Key 指定文本
/// @param font 修改的字体大小
/// @param color 修改的字体颜色
- (void)changeCharacterWithRepeatKey:(NSString *)Key Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color;


/// 改变文本中指定字符数组的颜色，字体（数组中的文本会全部查找出来修改）
/// @param KeyArray 修改数组（key数组）
/// @param font 字体大小
/// @param color 字体颜色
-(void)changeCharacterWithRepeatKeyArray:(NSArray *)KeyArray Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color;

/// 改变文本中指定字符数组的颜色，字体（数组中的文本只会查找出一次修改）
/// @param KeyArray 修改数组（key数组）
/// @param font 字体大小
/// @param color 字体颜色
-(void)changeCharacterWithArray:(NSArray *)KeyArray Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color;

/**
 添加链接字符

 @param string 指定某个字符为链接形式
 */
- (void)addLinkWithString:(NSString *)string;

/**
 自定义提示文本和提示文本字体颜色、大小
 */
- (instancetype)initWithFrame:(CGRect)frame placeHolderString:(NSString *)placeHolderString placeHoderColor:(UIColor *)color placHoderFont:(UIFont *)font;




@end

NS_ASSUME_NONNULL_END
