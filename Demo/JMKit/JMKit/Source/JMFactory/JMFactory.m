//
//  JMFactory.m
//  JMKit
//
//  Created by JM on 2019/6/24.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "JMFactory.h"
#import "JMHeader.h"

@implementation JMFactory

#pragma mark -- 文本类
+(JMLabel *)createLabelWithFont:(UIFont *)font{

    JMLabel *label = [[JMLabel alloc] init];
    label.font = font;
    
    return label;
    
}

+(JMLabel *)createLabelWithFont:(UIFont *)font textColor:(UIColor *)textColor{

    JMLabel *label = [[JMLabel alloc] init];
    label.font = font;
    label.textColor = textColor;
    
    return label;
    
}

+(JMLabel *)createLabelWithTitle:(NSString *)title Font:(UIFont *)font textColor:(UIColor *)textColor{

    JMLabel *label = [[JMLabel alloc] init];
    label.text = title;
    label.font = font;
    label.textColor = textColor;
    
    return label;
    
}

+(JMLabel *)createLabelWithTitle:(NSString *)title Font:(UIFont *)font textColor:(UIColor *)textColor textAligment:(NSTextAlignment)alignment{
    
    JMLabel *label = [[JMLabel alloc] init];
    label.text = title;
    label.font = font;
    label.textColor = textColor;
    label.textAlignment = alignment;
    
    return label;

}

+(JMLabel *)createLabelWithTitle:(NSString *)title Font:(UIFont *)font textColor:(UIColor *)textColor textAligment:(NSTextAlignment)alignment numberLines:(NSInteger)lines {
    
    JMLabel *label = [[JMLabel alloc] init];
    label.text = title;
    label.font = font;
    label.textColor = textColor;
    label.textAlignment = alignment;
    label.numberOfLines = lines;
    
    return label;

}


#pragma mark -- 创建按钮类
+(JMButton *)createButtonWithTitle:(nullable NSString *)title TitleColor:(nullable UIColor *)titleColor Font:(nullable UIFont *)font  target:(nullable id)target selector:(nullable SEL)selector{
    
    JMButton *button = [JMButton buttonWithType:UIButtonTypeCustom];
    
    if (title.length > 0) {
        
        [button setTitle:title forState:UIControlStateNormal];
    }
    
    if (titleColor != nil) {
        
        [button setTitleColor:titleColor forState:UIControlStateNormal];

    }else{
        
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
    }
    
    if (font != nil) {
        
        button.titleLabel.font = font;
        
    }
    
    if (target && selector) {
        
        [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    }

    return button;
}



+(JMButton *)createButtonWithBackGroundName:(nullable NSString *)imageName  target:(nullable id)target selector:(nullable SEL)selector{

    JMButton *button = [JMButton buttonWithType:UIButtonTypeCustom];
    
    if (imageName.length > 0) {
        [button setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }

    if (target && selector) {
        [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    }

    return button;

}

+(JMButton *)createButtonWithNorImageName:(nullable NSString *)norImageName  PreImageName:(nullable NSString *)preImageName target:(nullable id)target selector:(nullable SEL)selector{

    JMButton *button = [JMButton buttonWithType:UIButtonTypeCustom];

    if (target && selector) {
        [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    }

    if (norImageName.length > 0) {
        [button setImage:[UIImage imageNamed:norImageName] forState:UIControlStateNormal];
    }
    if (preImageName.length > 0) {
        [button setImage:[UIImage imageNamed:preImageName] forState:UIControlStateSelected];
    }


    return button;

}


+(JMButton *)createButtonWithTitle:(nullable NSString *)title TitleColor:(nullable UIColor *)titleColor Font:(nullable UIFont *)font NorImageName:(nullable NSString *)norImageName PreImageName:(nullable NSString *)preImageName target:(nullable id)target selector:(nullable SEL)selector{
    
    JMButton *button = [[JMButton alloc] init];
    
    button.titleLabel.font = font;
    
    if (titleColor != nil) {
        
        [button setTitleColor:titleColor forState:UIControlStateNormal];
    }
    if (title.length > 0) {
        
        [button setTitle:title forState:UIControlStateNormal];
    }

    if (norImageName.length > 0 ) {
        
        [button setImage:[UIImage imageNamed:norImageName] forState:UIControlStateNormal];
    }
    if (preImageName.length > 0) {
        
        [button setImage:[UIImage imageNamed:preImageName] forState:UIControlStateSelected];
    }
    if (target && selector) {
        
        [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    }


    return button;
}

#pragma mark -- 创建输入框
+(JMTextField *)createFieldWithTitle:(nullable NSString *)title TextFont:(UIFont *)textFont Placeholder:(nullable NSString *)placeholder{
    
    JMTextField *field = [[JMTextField alloc] init];
    field.font = textFont;
    if (placeholder.length > 0) {
        field.placeholder = placeholder;
    }
    if(title.length > 0){
        field.text = title;
    }

    return field;

}


+(JMTextField *)createFieldWithTextColor:(nullable UIColor *)textColor TextFont:(UIFont *)textFont Placeholder:(nullable NSString *)placeholder PlaceholderColor:(nullable UIColor *)placeholderColor PlaceholderFont:(nullable UIFont *)placeholderFont{

    JMTextField *field = [[JMTextField alloc] init];
    
    field.font = textFont;
    field.textColor = textColor;
        
    if (placeholder.length > 0) {
        field.placeholder = placeholder;
    }
    
    if (placeholderColor != nil) {
        field.placeholderColor = placeholderColor;
    }
    
    if (placeholderFont != nil) {
        field.placeholderFont = placeholderFont;
    }

    return field;

}


#pragma mark -- 创建textView

+(JMTextView *)createTextViewWithFrame:(CGRect)rect andTextViewFont:(CGFloat)fontSize{
    
    JMTextView *textView = [[JMTextView alloc] init];
    textView.frame = rect;
    textView.font = [UIFont systemFontOfSize:fontSize];
    
    return textView;
    
}



@end
