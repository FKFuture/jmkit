//
//  JMCustomAlertView.m
//  JMKitUpdate
//
//  Created by JM on 2022/2/10.
//  Copyright © 2022 YQ. All rights reserved.
//

#import "JMCustomAlertView.h"
#import "UIView+JMExtension.h"
#import "Masonry.h"
#import "JMFactory.h"
#import "JMLabel.h"
#import "JMButton.h"
#import "UIColor+JMExtension.h"

@interface JMCustomAlertView ()

@property(nonatomic,strong)NSMutableArray *blockArray;
@property(nonatomic,strong)NSMutableArray *buttonArray;
@property(nonatomic,strong)JMLabel *titleLabel;
@property(nonatomic,strong)JMLabel *suTitleLabel;
@property(nonatomic,strong)JMButton *containBgView;
@property(nonatomic,assign)BOOL subTitleIsInset;

@end

@implementation JMCustomAlertView


- (instancetype)initWithAlertTitle:(NSString *)title subTitle:(nullable NSString *)subTitle
{
    self = [super init];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
        self.frame = [UIScreen mainScreen].bounds;
        self.titleLabel.text = title;
        self.suTitleLabel.text = subTitle;
        
    }
    return self;
}

- (void)changeTitleWithFont:(UIFont *)font color:(UIColor *)color{
    
    self.titleLabel.font = font;
    self.titleLabel.textColor = color;
}

- (void)changeSuTitleWithFont:(UIFont *)font color:(UIColor *)color{
    
    self.suTitleLabel.font = font;
    self.suTitleLabel.textColor = color;
}

- (void)addCenterListAction:(NSArray <JMAlertAction *> *)actionLists corner:(CGFloat)cornerValue{
    
    self.containBgView = [JMButton new];
    self.containBgView.backgroundColor = [UIColor whiteColor];
    [self.containBgView setCornerRadius:cornerValue];
    
    UIView * topLine = [UIView new];
    topLine.backgroundColor = [UIColor colorWithHexString:@"#DFDFDF"];
    [self.containBgView addSubview:topLine];
    
    [topLine mas_makeConstraints:^(MASConstraintMaker *make) {
            
        make.left.right.offset(0);
        make.height.offset(1);
        make.bottom.offset(-44);
    }];

    
    [self addSubview:self.containBgView];
    [self.containBgView addSubview:self.titleLabel];
    [self.containBgView addSubview:self.suTitleLabel];
    
    [self.containBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.offset(52.5);
        make.right.offset(-52.5);
        make.center.offset(0);
            
    }];

    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.offset(36);
        make.bottom.offset(-80);
        make.left.offset(10);
        make.right.offset(-10);
            
    }];
    
    if (self.suTitleLabel.text.length > 0) {
        
        [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.offset(22);
            make.left.offset(10);
            make.right.offset(-10);
                
        }];
        
        [self.suTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(self.titleLabel.mas_bottom).offset(20);
            make.bottom.offset(-67.5);
            make.left.offset(20);
            make.right.offset(-20);
                
        }];
        
    }
    
    CGFloat rate = 1.0/actionLists.count;
    
    for (int i=0; i<actionLists.count; i++) {
        
        JMAlertAction *action = actionLists[i];
        
        JMButton *button = [JMFactory createButtonWithTitle:action.title TitleColor:action.titleColor Font:[UIFont systemFontOfSize:action.fontSize] target:self selector:@selector(actionClick:)];

        
        
        button.tag = i;
        [self.blockArray addObject:action.callblock];
        [self.buttonArray addObject:button];
        
        [self.containBgView addSubview:button];
        
        if (i<actionLists.count - 1) {
            
            UIView * rightLine = [UIView new];
            rightLine.backgroundColor = [UIColor colorWithHexString:@"#DFDFDF"];
            [button addSubview:rightLine];
            
            [rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
                    
                make.top.bottom.right.offset(0);
                make.width.offset(1);
            }];

        }
        
        
        if (i == 0) {
            
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.width.equalTo(self.containBgView.mas_width).multipliedBy(rate);
                make.height.offset(44);
                make.bottom.offset(0);
                make.left.offset(0);
                
            }];

        }else{
            
            JMButton *lastButton = self.buttonArray[i-1];
            
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.width.equalTo(self.containBgView.mas_width).multipliedBy(rate);
                make.height.offset(44);
                make.bottom.offset(0);
                make.left.equalTo(lastButton.mas_right).offset(0);

            }];

        }
        
        
    }
    
    
}



- (void)addBottomListAction:(NSArray <JMAlertAction *> *)actionLists {
    
    self.containBgView = [JMButton new];
    self.containBgView.backgroundColor = [UIColor whiteColor];
    
    [self addSubview:self.containBgView];
    [self.containBgView addSubview:self.titleLabel];
    [self.containBgView addSubview:self.suTitleLabel];
    
    [self.containBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.offset(10);
        make.right.offset(-10);
            
    }];
    
    [self initBottomTitleViewMasonry];
    
    
    CGFloat bottomMargin = 50;
    
    for (int i=0; i<actionLists.count; i++) {
        
        JMAlertAction *action = actionLists[i];
                
        JMButton *button = [JMFactory createButtonWithTitle:action.title TitleColor:action.titleColor Font:[UIFont systemFontOfSize:action.fontSize] target:self selector:@selector(actionClick:)];
        
        
        button.backgroundColor = [UIColor whiteColor];
        button.tag = i;
        
        [self.blockArray addObject:action.callblock];
        [self.buttonArray addObject:button];
        
        [self addSubview:button];
        
        if (i == actionLists.count - 1) {
            
            [self.containBgView mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.bottom.equalTo(button.mas_top).offset(-action.topMargin);
                
            }];
            
            if (action.topMargin > 0) {
                
                [self.containBgView setCornerRadius:action.cornerValue];

            }else{
                        
                [self.containBgView setCornerValue:action.cornerValue CornerType:UIRectCornerTopLeft | UIRectCornerTopRight];

            }
            
            

        }
        
        
        if (i == 0) {
            
            
            if (action.topMargin > 0 ) {
                
                
                [button setCornerRadius:action.cornerValue];

                
            }else{
                
                [button setCornerValue:action.cornerValue CornerType:UIRectCornerBottomLeft | UIRectCornerBottomRight ];
                

            }
            
                    
        }else{
            
            JMAlertAction *lastAction = actionLists[i-1];
            
            if (lastAction.topMargin > 0 && action.topMargin > 0 ) {
                
                [button setCornerRadius:action.cornerValue];

            }else if (lastAction.topMargin > 0 && action.topMargin <= 0 ){
                
                
                [button setCornerValue:action.cornerValue CornerType:UIRectCornerBottomLeft | UIRectCornerBottomRight ];


            }else if (lastAction.topMargin <= 0 && action.topMargin > 0 ){
                
                [button setCornerValue:action.cornerValue CornerType:UIRectCornerTopLeft | UIRectCornerTopRight ];

                
            }
            
            if (lastAction.topMargin <= 0) {
                
                //添加一条线
                UIView * bottomLine = [UIView new];
                bottomLine.backgroundColor = [UIColor colorWithHexString:@"#DFDFDF"];
                [button addSubview:bottomLine];
                
                [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
                        
                    make.left.bottom.right.offset(0);
                    make.height.offset(1);
                }];

                
            }
            
            if (action.topMargin <= 0 && i == actionLists.count - 1) {
                
                //添加一条线
                UIView * bottomLine = [UIView new];
                bottomLine.backgroundColor = [UIColor colorWithHexString:@"#DFDFDF"];
                [button addSubview:bottomLine];
                
                [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
                        
                    make.left.top.right.offset(0);
                    make.height.offset(1);
                }];

                
            }

            
            
        }
        
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.left.offset(action.paddMargin);
            make.right.offset(-action.paddMargin);
            make.bottom.offset(-bottomMargin);
            make.height.offset(action.height);

        }];
        
        bottomMargin += action.height + action.topMargin;


    
    }
    
    
}

- (void)initBottomTitleViewMasonry{
    

    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.offset(36);
        make.bottom.offset(-30);
        make.left.offset(10);
        make.right.offset(-10);
            
    }];
    
    if (self.suTitleLabel.text.length > 0) {
        
        [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.offset(22);
            make.left.offset(10);
            make.right.offset(-10);
                
        }];
        
        [self.suTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(self.titleLabel.mas_bottom).offset(20);
            make.bottom.offset(-17.5);
            make.left.offset(20);
            make.right.offset(-20);
                
        }];
        
    }

}



- (void)actionClick:(JMButton *)button{
    
    actionBlock block = self.blockArray[button.tag];
    
    block();
    
    [self close];
}


- (void)show{
    
    [UIApplication.sharedApplication.keyWindow addSubview:self];
}

- (void)close{
    
    [self removeFromSuperview];
}


- (JMLabel *)titleLabel{
    
    if (!_titleLabel) {
        _titleLabel = [JMLabel new];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    return _titleLabel;
}

- (JMLabel *)suTitleLabel{
    
    if (!_suTitleLabel) {
        _suTitleLabel = [JMLabel new];
        _suTitleLabel.numberOfLines = 0;
        _suTitleLabel.font = [UIFont systemFontOfSize:13];
        _suTitleLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    return _suTitleLabel;
}

- (NSMutableArray *)blockArray{
    
    if (!_blockArray) {
        _blockArray = [NSMutableArray arrayWithCapacity:0];
    }
    
    return _blockArray;
}

- (NSMutableArray *)buttonArray{
    
    if (!_buttonArray) {
        _buttonArray = [NSMutableArray arrayWithCapacity:0];
    }
    
    return _buttonArray;
}





@end

@implementation JMAlertAction

- (instancetype _Nonnull)initActionWithTitle:(NSString * _Nonnull )title  topMargin:(CGFloat)topMargin Block:(actionBlock _Nullable )block{
    
    self = [super init];
    
    if (self) {
        
        [self initData];
        self.title = title;
        self.topMargin = topMargin;
        self.callblock = block;
    }
    
    return self;
    
}

- (void)initData{
    
    self.height = 57;
    self.paddMargin = 10;
    self.topMargin = 10;
    self.cornerValue = 13;
    self.fontSize = 16;
    
}


@end

