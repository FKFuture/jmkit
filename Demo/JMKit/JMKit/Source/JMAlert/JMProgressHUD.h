//
//  JMProgressHUD.h
//  JMKitUpdate
//
//  Created by JM on 2019/10/13.
//  Copyright © 2019年 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JMProgressHUD : UIView

@property (assign, nonatomic) NSTimeInterval minShowTime;

+(instancetype)showHUDTo:(UIView *)view animated:(BOOL)animated;

+ (BOOL)hideHUDForView:(UIView *)view animated:(BOOL)animated;

- (void)hideAnimated:(BOOL)animated afterDelay:(NSTimeInterval)delay;

- (void)showAnimated:(BOOL)animated;
- (void)hideAnimated:(BOOL)animated;

/**
 设置标题颜色，大小，内容
 */
-(void)showTitle:(NSString *)title font:(UIFont *)font textColor:(UIColor *)textColor;

/**
 设置详细内容的颜色，大小，内容
 */
-(void)showDetailTitle:(NSString *)title font:(UIFont *)font textColor:(UIColor *)textColor;






@end

NS_ASSUME_NONNULL_END
