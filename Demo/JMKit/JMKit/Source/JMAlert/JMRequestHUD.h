//
//  JMRequestHUD.h
//  JMKitUpdate
//
//  Created by JM on 2019/10/13.
//  Copyright © 2019年 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JMRequestHUD : UIView

+ (JMRequestHUD*)sharedView;

/// 显示提示文本
/// @param title 文本内容
-(void)showStatusWithTitle:(NSString *)title duration:(NSTimeInterval)duration;

/// 显示提示文本（展示1.5秒）
/// @param title 显示内容
-(void)showTitleOnDefalutTime:(NSString *)title;


@end

NS_ASSUME_NONNULL_END
