//
//  JMCustomAlertView.h
//  JMKitUpdate
//
//  Created by JM on 2022/2/10.
//  Copyright © 2022 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^actionBlock)(void);

@class JMAlertAction;


NS_ASSUME_NONNULL_BEGIN

@interface JMCustomAlertView : UIView

/// 初始化弹窗
/// @param title 弹窗标题
/// @param subTitle 弹窗描述
- (instancetype)initWithAlertTitle:(NSString *)title subTitle:(nullable NSString *)subTitle ;

/// 修改弹窗标题字体大小和颜色
/// @param font 字体大小
/// @param color 字体颜色
- (void)changeTitleWithFont:(UIFont *)font color:(UIColor *)color;

/// 修改弹窗描述文本字体大小和颜色
/// @param font 字体大小
/// @param color 字体颜色
- (void)changeSuTitleWithFont:(UIFont *)font color:(UIColor *)color;

/// 添加事件列表
/// @param actionLists 事件列表
/// @param cornerValue 圆角值
- (void)addCenterListAction:(NSArray <JMAlertAction *> *)actionLists corner:(CGFloat)cornerValue;

/// 添加事件列表
/// @param actionLists 事件列表
- (void)addBottomListAction:(NSArray <JMAlertAction *> *)actionLists ;

/// 显示弹窗
- (void)show;

/// 关闭弹窗
- (void)close;

@end

NS_ASSUME_NONNULL_END


@interface JMAlertAction : NSObject

@property(nonatomic,copy)NSString * _Nonnull title;
@property(nonatomic,strong)UIColor * _Nullable titleColor;
@property(nonatomic,assign)CGFloat fontSize;
@property(nonatomic,assign)CGFloat height;
@property(nonatomic,assign)CGFloat paddMargin;
@property(nonatomic,assign)CGFloat topMargin;
@property(nonatomic,assign)CGFloat cornerValue;
@property(nonatomic,copy)actionBlock _Nonnull callblock;


/// 初始化事件action
/// @param title 文本内容
/// @param topMargin 底部间隔
/// @param block 回调block
- (instancetype _Nonnull)initActionWithTitle:(NSString * _Nonnull )title  topMargin:(CGFloat)topMargin Block:(actionBlock _Nullable )block;



@end

