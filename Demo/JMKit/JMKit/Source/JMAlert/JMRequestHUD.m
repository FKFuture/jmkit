//
//  JMRequestHUD.m
//  JMKitUpdate
//
//  Created by JM on 2019/10/13.
//  Copyright © 2019年 YQ. All rights reserved.
//

#import "JMRequestHUD.h"

@interface JMRequestHUD()

//定时器
@property (nonatomic, strong) NSTimer *graceTimer;
//弹窗消失定时器
@property (nonatomic, strong) NSTimer *fadeOutTimer;

//窗口等级
@property (assign, nonatomic) UIWindowLevel maxSupportedWindowLevel; // default is UIWindowLevelNormal

//视图最顶层的窗口
@property (nonatomic, readonly) UIWindow *frontWindow;

//控件背景视图
@property (nonatomic, strong) UIControl *controlView;

//背景视图
@property (nonatomic, strong) UIView *backGroundView;

/// 提示文本
@property (nonatomic,strong) UILabel *statusLabel;

@property (assign, nonatomic) NSTimeInterval graceTimeInterval;                             // default is 0 seconds
//弹窗最短消失时间
@property (assign, nonatomic) NSTimeInterval minimumDismissTimeInterval;                    // default is 5.0 seconds
//弹窗最长消失时间
@property (assign, nonatomic) NSTimeInterval maximumDismissTimeInterval;                    // default is CGFLOAT_MAX


//弹窗显示block
typedef void (^JMHUDShowCompletion)(void);
//弹窗消失block
typedef void (^JMHUDDismissCompletion)(void);



@end

@implementation JMRequestHUD

//单例创建hud
+ (JMRequestHUD*)sharedView {
    static dispatch_once_t once;
    
    static JMRequestHUD *sharedView;
    dispatch_once(&once, ^{ sharedView = [[self alloc] initWithFrame:[[UIScreen mainScreen] bounds]]; });
    
    return sharedView;
}

//初始化视图
- (instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];

    if (self) {

        self.userInteractionEnabled = NO;

        self.statusLabel = [[UILabel alloc] init];
        self.statusLabel.textColor = [UIColor whiteColor];
        self.statusLabel.font = [UIFont systemFontOfSize:14];
        self.statusLabel.numberOfLines = 0;

        //初始化参数
        _graceTimeInterval = 0.0f;
        _minimumDismissTimeInterval = 5.0f;
        _maximumDismissTimeInterval = CGFLOAT_MAX;

    }

    return self;

}

-(void)showTitleOnDefalutTime:(NSString *)title {

    __weak JMRequestHUD *weakSelf = self;

    [[NSOperationQueue mainQueue] addOperationWithBlock:^{

        __strong JMRequestHUD *strongSelf = weakSelf;

        if (strongSelf) {

            //销毁定时器
            strongSelf.fadeOutTimer = nil;
            strongSelf.graceTimer = nil;

            // Update / Check view hierarchy to ensure the HUD is visible
            [strongSelf updateViewHierarchy];

            strongSelf.statusLabel.text = title;


            if (self.graceTimeInterval > 0.0 ) {

                strongSelf.graceTimer = [NSTimer timerWithTimeInterval:self.graceTimeInterval target:strongSelf selector:@selector(fadeIn:) userInfo:@(1) repeats:NO];
                [[NSRunLoop mainRunLoop] addTimer:strongSelf.graceTimer forMode:NSRunLoopCommonModes];

            }else{

                [strongSelf fadeIn:@(1)];
            }


        }



    }];

}




//显示文本，设置显示时间时间
-(void)showStatusWithTitle:(NSString *)title duration:(NSTimeInterval)duration{

    __weak JMRequestHUD *weakSelf = self;

    [[NSOperationQueue mainQueue] addOperationWithBlock:^{

        __strong JMRequestHUD *strongSelf = weakSelf;

        if (strongSelf) {

            //销毁定时器
            strongSelf.fadeOutTimer = nil;
            strongSelf.graceTimer = nil;

            // Update / Check view hierarchy to ensure the HUD is visible
            [strongSelf updateViewHierarchy];

            strongSelf.statusLabel.text = title;


            if (self.graceTimeInterval > 0.0 ) {

                strongSelf.graceTimer = [NSTimer timerWithTimeInterval:self.graceTimeInterval target:strongSelf selector:@selector(fadeIn:) userInfo:@(duration) repeats:NO];
                [[NSRunLoop mainRunLoop] addTimer:strongSelf.graceTimer forMode:NSRunLoopCommonModes];

            }else{

                [strongSelf fadeIn:@(duration)];
            }


        }



    }];

}

- (void)fadeIn:(id)data{

    [self updateTitleRect];

    id duration = [data isKindOfClass:[NSTimer class]] ? ((NSTimer *)data).userInfo : data;

    if (duration) {
        self.fadeOutTimer = [NSTimer timerWithTimeInterval:[(NSNumber *)duration doubleValue] target:self selector:@selector(dismiss) userInfo:nil repeats:NO];

        [[NSRunLoop mainRunLoop] addTimer:self.fadeOutTimer forMode:NSRunLoopCommonModes];

    }






}

//弹窗消失
- (void)dismiss{

    [self dismissWithDelay:0.0 completion:nil];


}

//弹框消失和设置消失的block
- (void)dismissWithDelay:(NSTimeInterval)delay completion:(JMHUDDismissCompletion)completion {

    [self.backGroundView removeFromSuperview];
    

}



//更新提示文本尺寸
- (void)updateTitleRect{

    CGRect labelRect = CGRectZero;
    CGFloat labelHeight = 0.0f;
    CGFloat labelWidth = 0.0f;

    if (self.statusLabel.text) {

        CGSize constraintSize = CGSizeMake(200.0f, 300.0f);
        labelRect = [self.statusLabel.text boundingRectWithSize:constraintSize
                                                        options:(NSStringDrawingOptions)(NSStringDrawingUsesFontLeading | NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin)
                                                     attributes:@{NSFontAttributeName: self.statusLabel.font}
                                                        context:NULL];
        labelHeight = ceilf(CGRectGetHeight(labelRect));
        labelWidth = ceilf(CGRectGetWidth(labelRect));

    }

    self.statusLabel.frame = labelRect;

    self.controlView.frame = CGRectMake(0, 0, labelWidth+20, labelHeight+20);
    [self.controlView.layer setCornerRadius:4];
    self.controlView.layer.masksToBounds = YES;
    self.controlView.center = self.backGroundView.center;
    self.statusLabel.frame = CGRectMake(10, 10, labelWidth, labelHeight);

}


- (void)updateViewHierarchy{

    // Add the overlay to the application window if necessary

    [self.frontWindow addSubview:self.backGroundView];
    [self.backGroundView addSubview:self.controlView];
    [self.controlView addSubview:self.statusLabel];


}

- (UIControl *)controlView{

    if (!_controlView) {
        _controlView = [UIControl new];
        _controlView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _controlView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
        _controlView.userInteractionEnabled = YES;

    }

    return _controlView;
}

- (UIView *)backGroundView{

    if (!_backGroundView) {
        _backGroundView = [UIView new];
        _backGroundView.backgroundColor = [UIColor clearColor];
        _backGroundView.userInteractionEnabled = YES;
        _backGroundView.frame = [UIScreen mainScreen].bounds;
    }

    return _backGroundView;
}



- (UIWindow *)frontWindow {

    NSEnumerator *frontToBackWindows = [UIApplication.sharedApplication.windows reverseObjectEnumerator];

    for (UIWindow *window in frontToBackWindows) {

        BOOL windowOnMainScreen = window.screen == UIScreen.mainScreen;

        BOOL windowIsVisible = !window.hidden && window.alpha > 0;

        BOOL windowLevelSupported = (window.windowLevel >= UIWindowLevelNormal && window.windowLevel <= self.maxSupportedWindowLevel);

        BOOL windowKeyWindow = window.isKeyWindow;

        if(windowOnMainScreen && windowIsVisible && windowLevelSupported && windowKeyWindow) {
            return window;
        }

    }

    return nil;


}




@end
