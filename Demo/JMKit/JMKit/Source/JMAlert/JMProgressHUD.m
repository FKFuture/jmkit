//
//  JMProgressHUD.m
//  JMKitUpdate
//
//  Created by JM on 2019/10/13.
//  Copyright © 2019年 YQ. All rights reserved.
//

#import "JMProgressHUD.h"

static const CGFloat JMDefaultLabelFontSize = 16.f;
static const CGFloat JMDefaultDetailsLabelFontSize = 12.f;

//判断当前是否是主线程
#define JMMainThreadAssert() NSAssert([NSThread isMainThread], @"MBProgressHUD needs to be accessed on the main thread.");


@interface JMProgressHUD ()

@property(nonatomic,strong)UILabel *titleLabel; //标题文本
@property(nonatomic,strong)UILabel *detailLabel;//内容文本
@property(nonatomic,strong)UIView *backGroundView; //背景视图
@property(nonatomic,strong)UIView *controlBackView;//控件背景

@property (nonatomic, weak) NSTimer *graceTimer;
@property (nonatomic, weak) NSTimer *minShowTimer ;//最短显示时间定时器
@property (nonatomic, weak) NSTimer *hideDelayTimer; //延迟隐藏定时器
@property (nonatomic, weak) CADisplayLink *progressObjectDisplayLink;//精准定时器
@property (nonatomic, strong) NSDate *showStarted;     //弹框显示时间
@property (nonatomic, assign, getter=hasFinished) BOOL finished;//弹框显示是否结束



@end

@implementation JMProgressHUD

//添加视图到某视图上
+(instancetype)showHUDTo:(UIView *)view animated:(BOOL)animated{

    if (!view) {
        NSLog(@"view不能为空");
    }

    JMProgressHUD *hud = [[self alloc] initWithFrame:view.bounds];
    
    [view addSubview:hud];
    
    return hud;
    
}

//获取当前正在显示的提示弹框
+ (JMProgressHUD *)HUDForView:(UIView *)view {
    //逆向遍历子视图
    NSEnumerator *subviewsEnum = [view.subviews reverseObjectEnumerator];
    for (UIView *subview in subviewsEnum) {
        if ([subview isKindOfClass:self]) {
            JMProgressHUD *hud = (JMProgressHUD *)subview;
            if (hud.hasFinished == NO) {
                return hud;
            }
        }
    }
    return nil;
}


- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self initCommonView];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        
        [self initCommonView];
    }
    
    return self;
}

- (void)initCommonView{
    
    //设置视图的宽高与父视图自适应，如果父视图的宽高变化，自身也跟着变化
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

    //整个大背景
    self.backGroundView = [[UIView alloc] init];
    self.backGroundView.frame= self.bounds;
    self.backGroundView.backgroundColor = [UIColor clearColor];
    //UIViewAutoresizingFlexibleWidth          自动调整view的宽度，保证左边距和右边距不变
//    UIViewAutoresizingFlexibleHeight         自动调整view的高度，以保证上边距和下边距不变
    self.backGroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//    self.backGroundView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];

    
    [self addSubview:self.backGroundView];



    UIView *controlBackView = [[UIView alloc] init];
//    可以把 frame ，bouds，center 方式布局的视图自动转化为约束形式。（此时该视图上约束已经足够 不需要手动去添加别的约束
    controlBackView.center = self.backGroundView.center;
    controlBackView.translatesAutoresizingMaskIntoConstraints = NO;
    controlBackView.layer.cornerRadius = 5.f;
    controlBackView.backgroundColor = [UIColor colorWithWhite:0.f alpha:.2f];
    
    self.controlBackView = controlBackView;
    
    [self addSubview:self.controlBackView];

    //设置控件背景约束
    self.controlBackView.translatesAutoresizingMaskIntoConstraints = NO;

    //相当于controlBackView.center = self.center
    
    NSMutableArray *ControlBackConstraints = [NSMutableArray array];
    [ControlBackConstraints addObject:[NSLayoutConstraint constraintWithItem:self.controlBackView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
    [ControlBackConstraints addObject:[NSLayoutConstraint constraintWithItem:self.controlBackView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];


    [self addConstraints:ControlBackConstraints];


    UIColor *defaultColor = [UIColor colorWithWhite:0.f alpha:0.7f];


    UILabel *label = [UILabel new];

    label.adjustsFontSizeToFitWidth = NO;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = defaultColor;
    label.font = [UIFont boldSystemFontOfSize:JMDefaultLabelFontSize];
    label.opaque = NO;
    label.backgroundColor = [UIColor clearColor];
    self.titleLabel = label;
    [self.controlBackView addSubview:label];
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;

    NSMutableArray *titlesConstraints = [NSMutableArray array];

    [titlesConstraints addObject:[NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.controlBackView attribute:NSLayoutAttributeTop multiplier:1 constant:10]];
    [titlesConstraints addObject:[NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.controlBackView attribute:NSLayoutAttributeLeft multiplier:1 constant:10]];
    [titlesConstraints addObject:[NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationLessThanOrEqual toItem:self.controlBackView attribute:NSLayoutAttributeRight multiplier:1 constant:-10]];
    [titlesConstraints addObject:[NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.controlBackView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];


    [self.controlBackView addConstraints:titlesConstraints];


    UILabel *detailsLabel = [UILabel new];
    detailsLabel.adjustsFontSizeToFitWidth = NO;
    detailsLabel.textAlignment = NSTextAlignmentCenter;
    detailsLabel.textColor = defaultColor;
    detailsLabel.numberOfLines = 0;
    detailsLabel.font = [UIFont boldSystemFontOfSize:JMDefaultDetailsLabelFontSize];
    detailsLabel.opaque = NO;
    detailsLabel.backgroundColor = [UIColor clearColor];
    self.detailLabel = detailsLabel;
    
    [self.controlBackView addSubview:detailsLabel];

    self.detailLabel.translatesAutoresizingMaskIntoConstraints = NO;

    NSMutableArray *detailConstraints = [NSMutableArray array];

    [detailConstraints addObject:[NSLayoutConstraint constraintWithItem:self.detailLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.titleLabel attribute:NSLayoutAttributeBottom multiplier:1 constant:10]];
    [detailConstraints addObject:[NSLayoutConstraint constraintWithItem:self.detailLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.controlBackView attribute:NSLayoutAttributeLeft multiplier:1 constant:10]];
    [detailConstraints addObject:[NSLayoutConstraint constraintWithItem:self.detailLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationLessThanOrEqual toItem:self.controlBackView attribute:NSLayoutAttributeRight multiplier:1 constant:-10]];
    [detailConstraints addObject:[NSLayoutConstraint constraintWithItem:self.detailLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.controlBackView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
    [detailConstraints addObject:[NSLayoutConstraint constraintWithItem:self.detailLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.controlBackView attribute:NSLayoutAttributeBottom multiplier:1 constant:-10]];

    [self.controlBackView addConstraints:detailConstraints];



    

}

//设置提示文本和字体大小，颜色
-(void)showTitle:(NSString *)title font:(UIFont *)font textColor:(UIColor *)textColor{
    
    self.titleLabel.text = title;
    self.titleLabel.font = font;
    self.titleLabel.textColor = textColor;
    
}

-(void)showDetailTitle:(NSString *)title font:(UIFont *)font textColor:(UIColor *)textColor{

    self.detailLabel.text = title;
    self.detailLabel.font = font;
    self.detailLabel.textColor = textColor;

}


- (void)showAnimated:(BOOL)animated {
    
    //必须在主线程使用
    JMMainThreadAssert()
    
    //销毁定时器
    [self.minShowTimer invalidate];
    [self.hideDelayTimer invalidate];

    self.showStarted = [NSDate date];
    self.finished = NO;

}

//隐藏弹框
- (void)hideAnimated:(BOOL)animated {

    JMMainThreadAssert();
    self.finished = YES;

    //判断当前显示的最小时间显示器是否到了
    if (self.minShowTime > 0.0 && self.showStarted) {
        //获取当前已经显示的时间
        NSTimeInterval interv = [[NSDate date] timeIntervalSinceDate:self.showStarted];
        //如果当前还没到最小显示时间
        if (interv < self.minShowTime) {
            //创建剩余时间触发的定时器，赋值给最小显示时间定时器
            NSTimer *timer = [NSTimer timerWithTimeInterval:(self.minShowTime - interv) target:self selector:@selector(handleMinShowTimer:) userInfo:nil repeats:NO];
            [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
            self.minShowTimer = timer;
            return;
        }
    }

    [self done];

}

//设置延迟定时器
- (void)hideAnimated:(BOOL)animated afterDelay:(NSTimeInterval)delay {
    // Cancel any scheduled hideDelayed: calls
    [self.hideDelayTimer invalidate];

    NSTimer *timer = [NSTimer timerWithTimeInterval:delay target:self selector:@selector(handleHideTimer:) userInfo:@(animated) repeats:NO];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    self.hideDelayTimer = timer;
}


//最短时间定时器触发
- (void)handleMinShowTimer:(NSTimer *)theTimer {

    self.showStarted = nil;
    [self done];

}

//延迟定时器触发
- (void)handleHideTimer:(NSTimer *)timer {
    [self hideAnimated:[timer.userInfo boolValue]];
}


- (void)done {

    //延迟隐藏定时器销毁
    [self.hideDelayTimer invalidate];
    //精准定时器销毁

    if (self.hasFinished) {
        [self removeFromSuperview];
    }

}


//隐藏某个视图上的弹框
+ (BOOL)hideHUDForView:(UIView *)view animated:(BOOL)animated{
    
    JMProgressHUD *hud = [self HUDForView:view];
    if (hud != nil) {
        
        [hud hideAnimated:animated];
        return YES;
    }

    return NO;
    
}

//更新约束
- (void)layoutSubviews{

    [super layoutSubviews];



}

- (void)updateConstraints{


    [super updateConstraints];


}






@end
