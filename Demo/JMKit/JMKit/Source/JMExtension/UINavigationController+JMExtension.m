//
//  UINavigationController+JMExtension.m
//  JMKitUpdate
//
//  Created by JM on 2019/11/12.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "UINavigationController+JMExtension.h"

@implementation UINavigationController (JMExtension)

//设置导航栏背景颜色
-(void)setNavigationBackGroundColor:(UIColor *)color {

    //设置导航整体背景颜色，但是颜色值会有一些区别，要比原先颜色浅一些
    self.navigationBar.barTintColor = color;

}

//去掉导航背景设置颜色
-(void)dismissBackGroundWithColor:(UIColor *)color {

    //去掉导航背景颜色
    [self.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.shadowImage = [UIImage new];

    //设置导航颜色，但是状态栏没有颜色
    self.navigationBar.backgroundColor = color;
}

//设置背景色为系统颜色
-(void)recoverBackGroundColor{

    UINavigationBar *bar = [[UINavigationBar alloc]init];
    [self.navigationBar setBackgroundImage:bar.shadowImage  forBarMetrics:UIBarMetricsDefault];

}

-(void)setNavigationBarItemColor:(UIColor *)itemColor{

    self.navigationBar.tintColor = itemColor;

}

-(void)setNavigationBarTitleColor:(UIColor *)titleColor Font:(UIFont *)font {

    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];

    if (titleColor) {

        [dic setValue:titleColor forKey:NSForegroundColorAttributeName];
    }

    if (font) {

        [dic setValue:font forKey:NSFontAttributeName];


    }

    self.navigationBar.titleTextAttributes = dic.copy;

}

//导航返回只返回一个箭头
-(void)dismissNavigationBackDirection{

    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60) forBarMetrics:UIBarMetricsDefault];

}

- (void)setRightItemButton:(UIButton *)button rightMargin:(CGFloat)margin imageSize:(CGSize)size{

    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:button];

    self.navigationItem.rightBarButtonItem = item;

}


//func setRightItemButton(button:JMButton,rightMargin:CGFloat,imageSize:CGSize,buttonSize:CGSize)  {
//
//    button.imageRect = CGRect(x:buttonSize.width + 20 - rightMargin, y: buttonSize.height/2 - imageSize.height/2, width:imageSize.width, height: imageSize.height)
//    button.closeImageTitleMode = true
//    self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: button)
//}
//
//func setRightItemButton(button:JMButton,rightMargin:CGFloat,textSize:CGSize,buttonSize:CGSize)  {
//
//    button.titleRect = CGRect(x:20 - rightMargin, y: buttonSize.height/2 - textSize.height/2, width:textSize.width, height: textSize.height)
//    button.closeImageTitleMode = true
//    self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: button)
//}
//
//
//
////设置右侧按钮列表(按钮的大小需要与图片大小一致)
//func setRightItemsList(buttonArray:Array<JMButton>,ItemMarginArray:Array<CGFloat>,imageSizeArray:Array<CGSize>)  {
//
//    let  SystemItemArray :NSMutableArray = NSMutableArray()
//
//    for i in 0..<buttonArray.count{
//
//        let itemSpace = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
//        let button = buttonArray[i]
//
//        let customItem = UIBarButtonItem.init(customView:button)
//
//        let rightMargin = ItemMarginArray[i]
//        let imageSize = imageSizeArray[i]
//
//        button.closeImageTitleMode = true
//
//        if  i == 0 {
//
//            if  rightMargin <= 20{
//
//                //则图片需要向按钮外侧移动，移动距离为20 - rightMargin,则按钮可点击范围也需要扩大，否则会出现点击无效的情况
//                itemSpace.width = 0
//
//            }else{
//
//                itemSpace.width = rightMargin - 20
//            }
//
//            button.imageRect = CGRect(x: 20 - rightMargin, y: 0, width:imageSize.width, height: imageSize.height)
//
//        }
//
//        if i > 0 {
//
//            //获取上一位的margin
//            let lastItemMargin:CGFloat = ItemMarginArray[0]
//
//            var showX:CGFloat = 0
//
//            if lastItemMargin <=  CGFloat(20) {
//
//                showX =  20 - lastItemMargin
//
//            }else{
//
//                showX = 0
//            }
//
//            itemSpace.width = rightMargin
//
//            button.imageRect = CGRect(x:showX, y: 0, width:imageSize.width, height: imageSize.height)
//
//        }
//
//        SystemItemArray.add(itemSpace)
//        SystemItemArray.add(customItem)
//
//    }
//
//    self.navigationItem.rightBarButtonItems = (SystemItemArray as! [UIBarButtonItem])
//
//}



@end
