//
//  UILabel+JMExtension.m
//  JMKit
//
//  Created by JM on 2022/2/23.
//  Copyright © 2022 YQ. All rights reserved.
//

#import "UILabel+JMExtension.h"

@implementation UILabel (JMExtension)

/// 修改文本中指定字符串的字体大小/颜色（若文本中存在多个此文本会全部修改）
/// @param Key 指定文本
/// @param font 修改的字体大小
/// @param color 修改的字体颜色
- (void)changeCharacterWithRepeatKey:(NSString *)Key Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color{
        
    if (Key.length <= 0) return ;
    
    if (!font) {
        font = self.font;
    }
    if (!color){
        color = self.textColor;
    }
    
    NSMutableAttributedString *attributed = [self.attributedText mutableCopy];
        
    NSString *findString = self.text.copy;
    
    while ([findString rangeOfString:Key].location != NSNotFound) {
        
        NSRange range = [findString rangeOfString:Key];
        
        NSMutableString *replaceString = @"".mutableCopy;

        for (int i=0; i<range.length; i++) {
            
            [replaceString appendString:@" "];
        }
        
        findString = [findString stringByReplacingCharactersInRange:range withString:replaceString];
        

        [attributed setAttributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:font} range:range];

    }
    
    self.attributedText = attributed;

}

/// 改变文本中指定字符的颜色，字体(只限于查找到的第一个)
/// @param string 指定文本
/// @param font 修改的字体大小
/// @param color 修改的字体颜色
- (void)changeCharacterWithString:(NSString *)string  Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color{
    
    if (string.length <= 0) return ;
    
    if (!font) {
        font = self.font;
    }
    if (!color){
        color = self.textColor;
    }
    
    NSMutableAttributedString *attributed = [self.attributedText mutableCopy];
    
    NSRange range = [self.text rangeOfString:string];

    if (range.location != NSNotFound && font && color) {

        [attributed setAttributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:font} range:range];

        self.attributedText = attributed;

    }
    
}

/// 改变文本中指定字符数组的颜色，字体（数组中的文本会全部查找出来修改）
/// @param KeyArray 修改数组（key数组）
/// @param font 字体大小
/// @param color 字体颜色
-(void)changeCharacterWithRepeatKeyArray:(NSArray *)KeyArray Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color{

    for (int i=0; i<KeyArray.count; i++) {
        
        NSString *changeString = KeyArray[i];
        
        [self changeCharacterWithRepeatKey:changeString Font:font color:color];
                
    }
    
}

/// 改变文本中指定字符数组的颜色，字体（数组中的文本只会查找出一次修改）
/// @param KeyArray 修改数组（key数组）
/// @param font 字体大小
/// @param color 字体颜色
-(void)changeCharacterWithArray:(NSArray *)KeyArray Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color{

    for (int i=0; i<KeyArray.count; i++) {
        
        NSString *changeString = KeyArray[i];
        
        [self changeCharacterWithString:changeString Font:font color:color];
                
    }
    
}


/// 改变文本中指定范围的字体颜色大小
/// @param range 指定范围
/// @param font 字体大小
/// @param color 字体颜色
-(void)changeCharacterWithRang:(NSRange)range Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color {

    if (range.location == NSNotFound) return ;

    if (!font) {
        font = self.font;
    }

    if (!color){
        color = self.textColor;
    }
    NSMutableAttributedString *attributed = [self.attributedText mutableCopy];

        if (range.location != NSNotFound && font && color) {
            [attributed setAttributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:font} range:range];
        }


    self.attributedText = attributed;
}

//文本上添加横线,横线的颜色由字体颜色觉得
-(void)addLineToText:(NSString *)string {

    if (!string) return ;

    NSMutableAttributedString *attributed = [self.attributedText mutableCopy];

    NSRange range = [self.text rangeOfString:string];

    if (range.location != NSNotFound) {

        [attributed addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:range];
        self.attributedText = attributed;

    }

}

-(void)addDeleteLineToText:(NSString *)string {

    if (!string) return ;

    NSMutableAttributedString *attributed = [self.attributedText mutableCopy];

    NSRange range = [self.text rangeOfString:string];

    if (range.location != NSNotFound) {

        [attributed addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:range];
        self.attributedText = attributed;

    }

}


//添加下划线
- (void)addUnderLineToText:(NSString *)string lineColor:(UIColor *)lineColor underlineStyle:(NSUnderlineStyle)lineStyle{
    
    if (!string) return ;

    NSMutableAttributedString *attributed = [self.attributedText mutableCopy];

    NSRange range = [self.text rangeOfString:string];

    if (range.location != NSNotFound) {

//        [attributed addAttribute:NSUnderlineStyleAttributeName value:@( NSUnderlineStyleSingle) range:range];
        [attributed addAttribute:NSUnderlineStyleAttributeName value:@(lineStyle) range:range];

        [attributed addAttribute:NSUnderlineColorAttributeName value:lineColor range:range];

        self.attributedText = attributed;

    }

}

- (void)addBackGroundColorToText:(NSString *)string bgColor:(UIColor *)bgColor{
    
    if (!string) return ;

    NSMutableAttributedString *attributed = [self.attributedText mutableCopy];

    NSRange range = [self.text rangeOfString:string];

    if (range.location != NSNotFound) {

        [attributed addAttribute:NSBackgroundColorAttributeName value:bgColor range:range];

        self.attributedText = attributed;

    }

}


-(void)changeParagraphStyle:(NSMutableParagraphStyle *)style{
    
    
    NSMutableAttributedString *attributed = [self.attributedText mutableCopy];

    NSRange range = NSMakeRange(0, self.text.length);
    
    [attributed addAttribute:NSParagraphStyleAttributeName value:style range:range];
    self.attributedText = attributed;


}



@end
