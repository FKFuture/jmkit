//
//  UIView+JMExtension.h
//  JMKit
//
//  Created by JM on 2022/2/22.
//  Copyright © 2022 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JMCAGradientLayerView;

NS_ASSUME_NONNULL_BEGIN

@interface UIView (JMExtension)

@property (nonatomic, assign) CGFloat jm_x;

@property (nonatomic, assign) CGFloat jm_y;

@property (nonatomic, assign) CGFloat jm_right;

@property (nonatomic, assign) CGFloat jm_bottom;

@property (nonatomic, assign) CGFloat jm_width;

@property (nonatomic, assign) CGFloat jm_height;

@property (nonatomic, assign) CGSize jm_boundsSize;

@property (nonatomic, assign) CGPoint jm_frameOrigin;


#pragma mark --渐变相关

/** 自定义渐变层 */
@property (strong, nonatomic)JMCAGradientLayerView  *jm_GradientView;

/// 生成渐变色层
/// @param colors 渐变颜色数组
/// @param startPoint 开始点
/// @param endPoint 结束点
/// @param locations 颜色变化区间数组
+ (CAGradientLayer *)CAGradientWithColors:(nullable NSArray *)colors andStartPoint:(CGPoint)startPoint andEndPoint:(CGPoint)endPoint Locations:(nullable NSArray<NSNumber *> *)locations;

/// 初始化自定义渐变层（需要设置尺寸，设置jm_GradientView的frame或者）
/// @param layer 渐变层
- (void)initCustomCAGraient:(CAGradientLayer *)layer;

/// 初始化默认渐变层（不需要设置尺寸和约束，frame默认为当前视图的bounds）
/// @param layer 渐变层
- (void)initDefalutCAGraient:(CAGradientLayer *)layer;

/// 修改自定义渐变层
/// @param layer 渐变层
- (void)changeCustomCAGraient:(CAGradientLayer *)layer;


/// 移除自定义渐变色层
- (void)removeCustomCAGrdient;



#pragma mark --视图相关设置

/// 删除所有子元素
- (void)removeAllSubviews;

/// 移除指定类的控件
/// @param aClass 指定类
- (void)removeKindClass:(Class)aClass ;

/// 设置指定圆角
/// @param size 圆角尺寸
/// @param boundSize 当前控件尺寸
/// @param Corner 圆角类型
- (void)setCornerValueSize:(CGSize)size andBoundSize:(CGSize)boundSize andRectCorner:(UIRectCorner)Corner;

/// 设置圆角
- (void)setCornerRadius:(CGFloat)radius;

/// 设置边框颜色和宽度
- (void)setBorderWidth:(CGFloat)width color:(UIColor *)boarderColor;

/// 添加一条线到视图上，线的默认颜色值为#EAEAEA
/// @param frame 线的尺寸
- (void)addLineWithFrame:(CGRect)frame;

/// 添加一条线到视图上，指定线的颜色
/// @param frame 线的尺寸
/// @param color 线的颜色
- (void)addLineWithFrame:(CGRect)frame andLineColor:(UIColor *)color;

/**
 设置视图阴影和圆角大小(内容不超出圆角有效,适用于UIView)

 @param shadowColor 阴影颜色
 @param opacity 不透明度
 @param shadowRaius 阴影半径，阴影默认是圆形
 @param offsetSize 阴影在x，y上的偏移量（0,0）时以视图为中心点显示阴影
 @param cornerRadius 圆角大小
 */
- (void)addShadowColor:(UIColor *)shadowColor Opacity:(CGFloat)opacity shadowCorner:(CGFloat)shadowRaius Offset:(CGSize )offsetSize cornerRadius:(CGFloat)cornerRadius;


/**
 根据路径来设置视图阴影和圆角(内容不超出圆角区域有效，适用于UIView)
 @param shadowColor 阴影颜色
 @param opacity 不透明度
 @param path 阴影路径
 @param offsetSize 阴影偏移量
 @param cornerRadius 圆角大小
 */
- (void)addShadowColor:(UIColor *)shadowColor Opacity:(CGFloat)opacity Path:(UIBezierPath *)path Offset:(CGSize )offsetSize cornerRadius:(CGFloat)cornerRadius;


@end

NS_ASSUME_NONNULL_END
