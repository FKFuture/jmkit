//
//  UIView+JMExtension.m
//  JMKit
//
//  Created by JM on 2022/2/22.
//  Copyright © 2022 YQ. All rights reserved.
//

#import "UIView+JMExtension.h"

#import "UIColor+JMExtension.h"
#import "JMCAGradientLayerView.h"
#import <objc/runtime.h>
#import "Masonry.h"

//渐变层的key
static const char JMCAGradientViewKey = '\3';


@implementation UIView (JMExtension)

#pragma mark --渐变相关

/// 生成渐变色层
/// @param colors 渐变颜色数组
/// @param startPoint 开始点
/// @param endPoint 结束点
/// @param locations 颜色分布区间数组
+ (CAGradientLayer *)CAGradientWithColors:(nullable NSArray *)colors andStartPoint:(CGPoint)startPoint andEndPoint:(CGPoint)endPoint Locations:(nullable NSArray<NSNumber *> *)locations{
    
//    CGColorRef如何放入数组

    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.startPoint = startPoint;
    gl.endPoint = endPoint;
//    gl.colors = @[(__bridge id)startColor.CGColor,(__bridge id)endColor.CGColor];
    
    if (colors.count > 0) {
        gl.colors = colors;
    }
    
    if (locations.count > 0) {
        gl.locations = locations;
    }

    return gl;

}




- (void)setJm_GradientView:(JMCAGradientLayerView *)jm_GradientView{
    
    //存储新的
    objc_setAssociatedObject(self, &JMCAGradientViewKey,
                             jm_GradientView, OBJC_ASSOCIATION_RETAIN);

}

- (JMCAGradientLayerView *)jm_GradientView{
    
    return objc_getAssociatedObject(self, &JMCAGradientViewKey);

}


- (void)initCustomCAGraient:(CAGradientLayer *)layer{
    
    if (!self.jm_GradientView) {
        self.jm_GradientView = [[JMCAGradientLayerView alloc] init];
        self.jm_GradientView.userInteractionEnabled = NO;
    }
        
    self.jm_GradientView.jm_changeCALayer = layer;
    
    if (!CGRectEqualToRect(layer.frame, CGRectZero)) {
        self.jm_GradientView.frame = layer.frame;
    }
    
    [self insertSubview:self.jm_GradientView atIndex:0];

}

- (void)initDefalutCAGraient:(CAGradientLayer *)layer{
    
    [self initCustomCAGraient:layer];
    
    [self.jm_GradientView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.right.top.bottom.offset(0);
        
    }];
    

}


- (void)changeCustomCAGraient:(CAGradientLayer *)layer{
    
    self.jm_GradientView.jm_changeCALayer = layer;

    [self.jm_GradientView setNeedsDisplay];
    
}

- (void)removeCustomCAGrdient{
    
    [self.jm_GradientView removeFromSuperview];
    
}


#pragma mark --视图相关设置

- (CGFloat)jm_x{

    return self.frame.origin.x;
}

- (void)setJm_x:(CGFloat)jm_x{
    
    self.frame = CGRectMake(jm_x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
    
}

- (CGFloat)jm_y{
    
    return self.frame.origin.y;

}

- (void)setJm_y:(CGFloat)jm_y{
    
    self.frame = CGRectMake(self.frame.origin.x,
                            jm_y,
                            self.frame.size.width,
                            self.frame.size.height);

}


- (CGPoint)jm_frameOrigin
{
    return self.frame.origin;
}


- (void)setJm_frameOrigin:(CGPoint)jm_frameOrigin{
    
    self.frame = CGRectMake(jm_frameOrigin.x, jm_frameOrigin.y, self.frame.size.width, self.frame.size.height);

}

- (CGSize)jm_boundsSize
{
    return self.bounds.size;
}

- (void)setJm_boundsSize:(CGSize)jm_boundsSize{
    
    self.bounds = CGRectMake(self.bounds.origin.x,
                             self.bounds.origin.y,
                             jm_boundsSize.width,
                             jm_boundsSize.height);

}


- (CGFloat)jm_width
{
    return self.frame.size.width;
}

- (void)setJm_width:(CGFloat)jm_width
{
    self.frame = CGRectMake(self.frame.origin.x,
                             self.frame.origin.y,
                             jm_width,
                             self.frame.size.height);
}

- (CGFloat)jm_height
{
    return self.frame.size.height;
}

- (void)setJm_height:(CGFloat)jm_height
{
    self.frame = CGRectMake(self.frame.origin.x,
                            self.frame.origin.y,
                            self.frame.size.width,
                            jm_height);
}

- (CGFloat)jm_bottom{

    return self.frame.origin.y + self.frame.size.height;
}

- (void)setJm_bottom:(CGFloat)jm_bottom{
    
    self.frame = CGRectMake(self.frame.origin.x,
                            jm_bottom - self.jm_height,
                            self.frame.size.width,
                            self.frame.size.height);

}

- (CGFloat)jm_right{
    
    return self.jm_x + self.jm_width;

}

- (void)setJm_right:(CGFloat)jm_right{
    
    self.frame = CGRectMake(jm_right - self.jm_width,
                            self.frame.origin.y,
                            self.jm_width,
                            self.jm_height);

    
}


- (void)removeAllSubviews {
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
}

- (void)removeKindClass:(Class)aClass {
    
    for (UIView *view in self.subviews) {
        
        if ([view isKindOfClass:aClass]) {
            [view removeFromSuperview];
        }
    }
        
}

/// 设置指定圆角
/// @param size 圆角尺寸
/// @param boundSize 当前控件尺寸
/// @param Corner 圆角类型
- (void)setCornerValueSize:(CGSize)size andBoundSize:(CGSize)boundSize andRectCorner:(UIRectCorner)Corner{

    CGRect boundRect = CGRectMake(0, 0,boundSize.width ,boundSize.height);

    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:boundRect byRoundingCorners:Corner cornerRadii:size];

    CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];

    //设置大小
    maskLayer.frame = boundRect;
    maskLayer.path = maskPath.CGPath;

    self.layer.mask = maskLayer;

}



/// 设置圆角
- (void)setCornerRadius:(CGFloat)radius
{
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = radius;
}


/// 设置边框
- (void)setBorderWidth:(CGFloat)width color:(UIColor *)borderColor
{
    self.layer.borderWidth = width;
    self.layer.borderColor = borderColor.CGColor;
}

- (void)addLineWithFrame:(CGRect)frame{

    CALayer *layer = [[CALayer alloc] init];
    layer.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"].CGColor;
    layer.frame = frame;

    [self.layer addSublayer:layer];

}

- (void)addLineWithFrame:(CGRect)frame andLineColor:(UIColor *)color{

    CALayer *layer = [[CALayer alloc] init];

    if (color) {
        layer.backgroundColor = color.CGColor;
    }else{
        layer.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"].CGColor;
    }
    layer.frame = frame;
    [self.layer addSublayer:layer];

}


//设置视图的阴影效果
- (void)addShadowColor:(UIColor *)shadowColor Opacity:(CGFloat)opacity shadowCorner:(CGFloat)shadowRaius Offset:(CGSize )offsetSize cornerRadius:(CGFloat)cornerRadius {

    /*默认阴影值，默认阴影颜色是透明色
    shadowOffset = CGSizeMake(0, -3)
    shadowRadius = 3.0
    */

    self.layer.shadowColor = shadowColor.CGColor;
    self.layer.shadowOpacity = opacity;
    self.layer.shadowRadius = shadowRaius;
    self.layer.shadowOffset = offsetSize;
    self.layer.cornerRadius = cornerRadius;


}

//根据路径来设置阴影
- (void)addShadowColor:(UIColor *)shadowColor Opacity:(CGFloat)opacity Path:(UIBezierPath *)path Offset:(CGSize )offsetSize cornerRadius:(CGFloat)cornerRadius{
    
    self.layer.shadowColor = shadowColor.CGColor;
    self.layer.shadowOpacity = opacity;
    self.layer.shadowPath = path.CGPath;
    self.layer.shadowOffset = offsetSize;
    self.layer.cornerRadius = cornerRadius;

}



@end
