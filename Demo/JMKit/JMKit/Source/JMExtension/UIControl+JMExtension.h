//
//  UIControl+JMExtension.h
//  JMKit
//
//  Created by JM on 2022/2/23.
//  Copyright © 2022 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIControl (JMExtension)

// 可以用这个给重复点击加间隔 页面跳转暂为1秒  可连续点击暂为0.25秒
@property (nonatomic, assign) NSTimeInterval custom_acceptEventInterval;

@end

NS_ASSUME_NONNULL_END
