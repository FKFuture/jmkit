//
//  UINavigationController+JMExtension.h
//  JMKitUpdate
//
//  Created by JM on 2019/11/12.
//  Copyright © 2019 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UINavigationController (JMExtension)

/**
 设置导航栏背景颜色(背景图片还在，颜色比预定浅)
 @param color 设定颜色
 */
-(void)setNavigationBackGroundColor:(UIColor *)color ;

/**
 去掉导航背景设置颜色

 @param color 设定颜色
 */
-(void)dismissBackGroundWithColor:(UIColor *)color;


/**
 恢复导航栏为系统默认颜色
 */
-(void)recoverBackGroundColor;


/**
 设置导航控件颜色

 @param itemColor 导航控件颜色
 */
-(void)setNavigationBarItemColor:(UIColor *)itemColor;


/**
 设置导航标题颜色，字体大小

 @param titleColor 标题颜色
 @param font 字体大小
 */
-(void)setNavigationBarTitleColor:(UIColor *)titleColor Font:(UIFont *)font ;


/**
 设置导航左侧按钮只有返回箭头
 */
-(void)dismissNavigationBackDirection;


@end

NS_ASSUME_NONNULL_END
