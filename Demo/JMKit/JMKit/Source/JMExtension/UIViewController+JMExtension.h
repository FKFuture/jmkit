//
//  UIViewController+JMExtension.h
//  JMKitUpdate
//
//  Created by JM on 2019/11/12.
//  Copyright © 2019 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (JMExtension)

 
/// 系统标准弹窗
/// @param title 标题
/// @param subTitle 副标题
/// @param mode 弹窗模式
/// @param cancleBlock 取消block
/// @param sureBlock 确定block
/// @param completeBlock 完成block
- (void)showAlertWithTitle:(NSString *)title subTitle:(NSString *)subTitle mode:(UIAlertControllerStyle)mode cancleBlock:(void(^)(void))cancleBlock sureBlock:(void(^)(void))sureBlock completion:(void(^)(void))completeBlock;

/// 系统标准弹窗
/// @param title 标题
/// @param subTitle 副标题
/// @param mode 弹窗模式
/// @param actionList UIAlertAction列表数组
/// @param completeBlock 完成block
- (void)showAlertWithTitle:(NSString *)title subTitle:(NSString *)subTitle mode:(UIAlertControllerStyle)mode AlertActionList:(NSArray <UIAlertAction *>*)actionList completion:(void(^)(void))completeBlock;


/// 返回到指定的控制器，若未找到则返回上一级
/// @param limitClass 指定控制器类
- (void)backToLimitController:(Class )limitClass;
    

/// 返回过滤掉指定控制器至当前控制器的所有控制器,返回到指定控制器上一级
/// @param igoredClass 需要忽略的控制器类
- (void)backIgoredController:(Class )igoredClass;




@end

NS_ASSUME_NONNULL_END
