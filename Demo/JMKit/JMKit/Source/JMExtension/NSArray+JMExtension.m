//
//  NSArray+JMExtension.m
//  JMKitUpdate
//
//  Created by JM on 2022/2/14.
//  Copyright © 2022 YQ. All rights reserved.
//

#import "NSArray+JMExtension.h"
#import "Masonry.h"

@implementation NSArray (JMExtension)

-(void)makeVerticalDirectionWithFixSpacing:(CGFloat)fixSpacing leadSpacing:(CGFloat)leadSpacing tailSpacing:(CGFloat)tailSpacing itemleftSpacing:(CGFloat)leftSpacing itemRightSpacing:(CGFloat)rightSpacing
{
    
    for (int i=0; i<self.count; i++) {
        
        UIView *subView = self[i];
        
        [subView mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.left.offset(leftSpacing);
            make.right.offset(-rightSpacing);

        }];
        
        if (i == 0) {
            
            [subView mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.top.offset(leadSpacing);
                
            }];
        }
        
        if (i == self.count - 1) {
            
            [subView mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.bottom.offset(-tailSpacing);
                
            }];
        }
        
        if (i > 0) {
            
            UIView *lastView = self[i-1];
            
            [subView mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.top.equalTo(lastView.mas_bottom).offset(fixSpacing);
                
            }];

        }

        
    }
    
}

-(void)makeHorizontalDirectionWithFixSpacing:(CGFloat)fixSpacing leadSpacing:(CGFloat)leadSpacing tailSpacing:(CGFloat)tailSpacing itemTopSpacing:(CGFloat)topSpacing itemBottomSpacing:(CGFloat)bottomSpacing
{
    
    for (int i=0; i<self.count; i++) {
        
        UIView *subView = self[i];
        
        [subView mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.offset(topSpacing);
            make.bottom.offset(-bottomSpacing);

        }];
        
        if (i == 0) {
            
            [subView mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.left.offset(leadSpacing);
                
            }];
        }
        
        if (i == self.count - 1) {
            
            [subView mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.right.offset(-tailSpacing);
                
            }];
        }
        
        if (i > 0) {
            
            UIView *lastView = self[i-1];
            
            [subView mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.left.equalTo(lastView.mas_right).offset(fixSpacing);
                
            }];

        }

        
    }
    
}




@end
