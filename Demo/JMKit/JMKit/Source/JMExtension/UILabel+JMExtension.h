//
//  UILabel+JMExtension.h
//  JMKit
//
//  Created by JM on 2022/2/23.
//  Copyright © 2022 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (JMExtension)

/// 改变文本中指定字符的颜色，字体(只限于查找到的第一个)
/// @param string 指定文本
/// @param font 修改的字体大小
/// @param color 修改的字体颜色
- (void)changeCharacterWithString:(NSString *)string  Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color;

/// 修改文本中指定字符串的字体大小/颜色（若文本中存在多个此文本会全部修改）
/// @param Key 指定文本
/// @param font 修改的字体大小
/// @param color 修改的字体颜色
- (void)changeCharacterWithRepeatKey:(NSString *)Key Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color;


/// 改变文本中指定字符数组的颜色，字体（数组中的文本会全部查找出来修改）
/// @param KeyArray 修改数组（key数组）
/// @param font 字体大小
/// @param color 字体颜色
-(void)changeCharacterWithRepeatKeyArray:(NSArray *)KeyArray Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color;

/// 改变文本中指定字符数组的颜色，字体（数组中的文本只会查找出一次修改）
/// @param KeyArray 修改数组（key数组）
/// @param font 字体大小
/// @param color 字体颜色
-(void)changeCharacterWithArray:(NSArray *)KeyArray Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color;

/// 改变文本中指定范围的字体颜色大小
/// @param range 指定范围
/// @param font 字体大小
/// @param color 字体颜色
-(void)changeCharacterWithRang:(NSRange)range Font:(UIFont *_Nullable)font color:(UIColor *_Nullable)color ;


/// 在指定文本上添加横线
/// @param string 指定文本
-(void)addLineToText:(NSString *)string;

/// 在指定文本添加下划线
/// @param string 指定内容
/// @param lineColor 下划线颜色
/// @param lineStyle 下划线样式
- (void)addUnderLineToText:(NSString *)string lineColor:(UIColor *)lineColor underlineStyle:(NSUnderlineStyle)lineStyle;

/// 设置指定文本背景色
/// @param string 指定文本
/// @param bgColor 背景颜色
- (void)addBackGroundColorToText:(NSString *)string bgColor:(UIColor *)bgColor;

/// 设置文本的段落样式
/// @param style 段落样式
-(void)changeParagraphStyle:(NSMutableParagraphStyle *)style;


@end

NS_ASSUME_NONNULL_END
