//
//  JMCAGradientLayerView.m
//  JMKit
//
//  Created by JM on 2022/2/17.
//  Copyright © 2022 YQ. All rights reserved.
//

#import "JMCAGradientLayerView.h"
#import "UIView+JMExtension.h"


@implementation JMCAGradientLayerView

- (void)drawRect:(CGRect)rect{
    
    [super drawRect:rect];
    
    if (self.jm_changeCALayer) {
        self.jm_changeCALayer.frame = self.bounds;
    }
}


- (void)setJm_changeCALayer:(CAGradientLayer *)jm_changeCALayer{
    
    if (_jm_changeCALayer != jm_changeCALayer) {
        [_jm_changeCALayer removeFromSuperlayer];
    }
    
    _jm_changeCALayer = jm_changeCALayer;
    [self.layer insertSublayer:_jm_changeCALayer atIndex:0];

}

@end
