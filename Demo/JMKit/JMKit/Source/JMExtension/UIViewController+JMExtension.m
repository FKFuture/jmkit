//
//  UIViewController+JMExtension.m
//  JMKitUpdate
//
//  Created by JM on 2019/11/12.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "UIViewController+JMExtension.h"

@implementation UIViewController (JMExtension)

- (void)showAlertWithTitle:(NSString *)title subTitle:(NSString *)subTitle mode:(UIAlertControllerStyle)mode cancleBlock:(void(^)(void))cancleBlock sureBlock:(void(^)(void))sureBlock completion:(void(^)(void))completeBlock{

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:subTitle preferredStyle:mode];

    UIAlertAction *sureAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        if (sureBlock) {
            sureBlock();
        }

    }];

    UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {

        if (cancleBlock) {
            cancleBlock();
        }

    }];

    [alertController addAction:sureAction];
    [alertController addAction:cancleAction];

    [self presentViewController:alertController animated:YES completion:^{

        if (completeBlock) {
            completeBlock();
        }

    }];

}


- (void)showAlertWithTitle:(NSString *)title subTitle:(NSString *)subTitle mode:(UIAlertControllerStyle)mode AlertActionList:(NSArray <UIAlertAction *>*)actionList completion:(void(^)(void))completeBlock{

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:subTitle preferredStyle:mode];
    
    [actionList enumerateObjectsUsingBlock:^(UIAlertAction * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [alertController addAction:obj];

    }];
    
    [self presentViewController:alertController animated:YES completion:^{

        if (completeBlock) {
            completeBlock();
        }

    }];

}


/// 返回到指定的控制器，若未找到则返回上一级
/// @param limitClass 指定控制器类
- (void)backToLimitController:(Class )limitClass{
    
    NSArray<UIViewController *> *viewControllers = [NSArray arrayWithArray:self.navigationController.viewControllers];
    
    if (viewControllers.count <= 1) {
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }
        
    for (UIViewController *vc in viewControllers) {
        
        if ([vc isKindOfClass:limitClass]) {
            
            [self.navigationController popToViewController:vc animated:YES];
            
            return;
            
        }
        
    }
    
    [self.navigationController popViewControllerAnimated:YES];

}


/// 返回过滤掉指定控制器至当前控制器的所有控制器,返回到指定控制器上一级
/// @param igoredClass 需要忽略的控制器类
- (void)backIgoredController:(Class )igoredClass{
    
    NSArray<UIViewController *> *viewControllers = [NSArray arrayWithArray:self.navigationController.viewControllers];
    NSMutableArray<UIViewController *> *targetViewControllers = [NSMutableArray array];
    for (int i = 0; i<viewControllers.count; i++ ) {
        
        UIViewController *vc = viewControllers[i];
        
        if ([vc isKindOfClass:igoredClass]) {
            break;
        }else if (vc == self) {
            break;
        }
        [targetViewControllers addObject:vc];
    }
        
    [targetViewControllers addObject:self];
    [self.navigationController setViewControllers:targetViewControllers];
    [self.navigationController popViewControllerAnimated:YES];

}







@end
