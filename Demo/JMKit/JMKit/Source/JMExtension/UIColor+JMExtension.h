//
//  UIColor+JMExtension.h
//  JMKit
//
//  Created by JM on 2022/2/23.
//  Copyright © 2022 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (JMExtension)

/// 根据颜色值返回颜色
/// @param color 颜色值小于六位，返回透明色,不等于六位返回白色
+ (UIColor *) colorWithHexString: (NSString *)color;

@end

NS_ASSUME_NONNULL_END
