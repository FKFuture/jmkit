//
//  NSArray+JMExtension.h
//  JMKitUpdate
//
//  Created by JM on 2022/2/14.
//  Copyright © 2022 YQ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSArray (JMExtension)

/// 设置竖直方向的约束数组
/// @param fixSpacing item之间的间隙
/// @param leadSpacing 第一个视图和父视图的顶部间隙
/// @param tailSpacing 最后一个视图和父视图的底部间隙
/// @param leftSpacing 所有item左边离父视图左边的间隙
/// @param rightSpacing 所有item右边离父视图右边的间隙
-(void)makeVerticalDirectionWithFixSpacing:(CGFloat)fixSpacing leadSpacing:(CGFloat)leadSpacing tailSpacing:(CGFloat)tailSpacing itemleftSpacing:(CGFloat)leftSpacing itemRightSpacing:(CGFloat)rightSpacing;

/// 设置水平方向的约束数组
/// @param fixSpacing item之间的间隙
/// @param leadSpacing 第一个视图和父视图的左边间隙
/// @param tailSpacing 最后一个视图和父视图的右边间隙
/// @param topSpacing 所有item顶部离父视图顶部的间隙
/// @param bottomSpacing 所有item底部离父视图底部的间隙
-(void)makeHorizontalDirectionWithFixSpacing:(CGFloat)fixSpacing leadSpacing:(CGFloat)leadSpacing tailSpacing:(CGFloat)tailSpacing itemTopSpacing:(CGFloat)topSpacing itemBottomSpacing:(CGFloat)bottomSpacing;


@end

NS_ASSUME_NONNULL_END
