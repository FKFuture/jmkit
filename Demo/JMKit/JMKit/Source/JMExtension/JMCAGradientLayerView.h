//
//  JMCAGradientLayerView.h
//  JMKit
//
//  Created by JM on 2022/2/17.
//  Copyright © 2022 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/// 渐变层视图
@interface JMCAGradientLayerView : UIView

/// 渐变层
@property(nonatomic,strong)CAGradientLayer *jm_changeCALayer;

@end

NS_ASSUME_NONNULL_END
