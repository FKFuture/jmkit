//
//  JMRefreshStatusHeader.m
//  JMKitUpdate
//
//  Created by JM on 2019/10/22.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "JMRefreshStatusHeader.h"
#import "UIView+JMExtension.h"

@interface JMRefreshStatusHeader ()
{

    /** 显示上一次刷新时间的label */
    __unsafe_unretained UILabel *_lastUpdatedTimeLabel;
    /** 显示刷新状态的label */
    __unsafe_unretained UILabel *_stateLabel;

}

@property (strong, nonatomic) NSMutableDictionary *stateTitles;


@end

@implementation JMRefreshStatusHeader



#pragma mark - 懒加载
- (NSMutableDictionary *)stateTitles
{
    if (!_stateTitles) {
        self.stateTitles = [NSMutableDictionary dictionary];
    }
    return _stateTitles;
}

- (UILabel *)stateLabel
{
    if (!_stateLabel) {
        [self addSubview:_stateLabel = [self mj_label]];
    }
    return _stateLabel;
}


- (UILabel *)mj_label{

    UILabel *label = [[UILabel alloc] init];
    label.font = [UIFont boldSystemFontOfSize:14];
    label.textColor = [UIColor redColor];
    label.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    return label;
}


- (UILabel *)lastUpdatedTimeLabel
{
    if (!_lastUpdatedTimeLabel) {
        [self addSubview:_lastUpdatedTimeLabel = [self mj_label]];
    }
    return _lastUpdatedTimeLabel;
}

#pragma mark - 公共方法
- (void)setTitle:(NSString *)title forState:(JMRefreshState)state
{
    if (title == nil) return;
    self.stateTitles[@(state)] = title;
    self.stateLabel.text = self.stateTitles[@(self.state)];
}

#pragma mark - 日历获取在9.x之后的系统使用currentCalendar会出异常。在8.0之后使用系统新API。
- (NSCalendar *)currentCalendar {
    if ([NSCalendar respondsToSelector:@selector(calendarWithIdentifier:)]) {
        return [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    }
    return [NSCalendar currentCalendar];
}

#pragma mark key的处理
- (void)setLastUpdatedTimeKey:(NSString *)lastUpdatedTimeKey
{
    [super setLastUpdatedTimeKey:lastUpdatedTimeKey];

    // 如果label隐藏了，就不用再处理
    if (self.lastUpdatedTimeLabel.hidden) return;

    NSDate *lastUpdatedTime = [[NSUserDefaults standardUserDefaults] objectForKey:lastUpdatedTimeKey];

    // 如果有block
    if (self.lastUpdatedTimeText) {
        self.lastUpdatedTimeLabel.text = self.lastUpdatedTimeText(lastUpdatedTime);
        return;
    }

    if (lastUpdatedTime) {
        // 1.获得年月日
        NSCalendar *calendar = [self currentCalendar];
        NSUInteger unitFlags = NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay |NSCalendarUnitHour |NSCalendarUnitMinute;
        NSDateComponents *cmp1 = [calendar components:unitFlags fromDate:lastUpdatedTime];
        NSDateComponents *cmp2 = [calendar components:unitFlags fromDate:[NSDate date]];

        // 2.格式化日期
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        BOOL isToday = NO;
        if ([cmp1 day] == [cmp2 day]) { // 今天
            formatter.dateFormat = @" HH:mm";
            isToday = YES;
        } else if ([cmp1 year] == [cmp2 year]) { // 今年
            formatter.dateFormat = @"MM-dd HH:mm";
        } else {
            formatter.dateFormat = @"yyyy-MM-dd HH:mm";
        }
        NSString *time = [formatter stringFromDate:lastUpdatedTime];

        // 3.显示日期
        self.lastUpdatedTimeLabel.text = [NSString stringWithFormat:@"%@%@%@",
                                          @"上次刷新时间",
                                          isToday ? @"当前刷新时间" : @"",
                                          time];
    } else {
        self.lastUpdatedTimeLabel.text = [NSString stringWithFormat:@"%@%@",
                                          @"上次刷新时间",
@"最后一次刷新时间"];
    }
}

#pragma mark - 覆盖父类的方法
- (void)prepare
{
    [super prepare];

    // 初始化间距
    self.labelLeftInset = 25;

    // 初始化文字
    [self setTitle:@"闲置状态刷新完毕" forState:JMRefreshStateIdle];
    [self setTitle:@"下拉看到更多小哥哥哦" forState:JMRefreshStatePulling];
    [self setTitle:@"正在刷新中" forState:JMRefreshStateRefreshing];
}

- (void)placeSubviews
{
    [super placeSubviews];

    if (self.stateLabel.hidden) return;

    BOOL noConstrainsOnStatusLabel = self.stateLabel.constraints.count == 0;

    if (self.lastUpdatedTimeLabel.hidden) {
        // 状态
        if (noConstrainsOnStatusLabel) self.stateLabel.frame = self.bounds;
    } else {
        CGFloat stateLabelH = self.jm_height * 0.5;
        // 状态
        if (noConstrainsOnStatusLabel) {
            self.stateLabel.jm_x = 0;
            self.stateLabel.jm_y = 0;
            self.stateLabel.jm_width = self.jm_width;
            self.stateLabel.jm_height = stateLabelH;
        }

        // 更新时间
        if (self.lastUpdatedTimeLabel.constraints.count == 0) {
            self.lastUpdatedTimeLabel.jm_x = 0;
            self.lastUpdatedTimeLabel.jm_y = stateLabelH;
            self.lastUpdatedTimeLabel.jm_width = self.jm_width;
            self.lastUpdatedTimeLabel.jm_height = self.jm_height - self.lastUpdatedTimeLabel.jm_y;
        }
    }
}

- (void)setState:(JMRefreshState)state
{

    JMRefreshState oldState = self.state; \
    if (state == oldState) return; \
    [super setState:state];


    // 设置状态文字
    self.stateLabel.text = self.stateTitles[@(state)];

    // 重新设置key（重新显示时间）
    self.lastUpdatedTimeKey = self.lastUpdatedTimeKey;
}

@end
