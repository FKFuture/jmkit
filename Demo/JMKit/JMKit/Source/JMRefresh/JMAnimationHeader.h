//
//  JMAnimationHeader.h
//  JMKitUpdate
//
//  Created by JM on 2019/10/23.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "JMRefreshHeader.h"

NS_ASSUME_NONNULL_BEGIN

@interface JMAnimationHeader : JMRefreshHeader

@property (weak, nonatomic, readonly) UILabel *animationLabel;
@property (weak, nonatomic, readonly) UIImageView *statusImageView;


@end

NS_ASSUME_NONNULL_END
