//
//  JMBaseRefreshView.m
//  JMKitUpdate
//
//  Created by JM on 2019/10/21.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "JMBaseRefreshView.h"

#import "UIView+JMExtension.h"
#import "UIScrollView+JMExtension.h"
#import "constant.h"

@interface JMBaseRefreshView()

@property (nonatomic,strong) UIPanGestureRecognizer *pan;

@end

@implementation JMBaseRefreshView

- (instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];

    if (self) {

        [self prepare];
        self.state = JMRefreshStateIdle;

    }

    return self;
}

- (void)prepare
{
    // 基本属性
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.backgroundColor = [UIColor clearColor];
}


//布局视图
- (void)layoutSubviews{

    [self placeSubviews];

    [super layoutSubviews];
}

- (void)placeSubviews{}

//当视图即将加入父视图的时候
- (void)willMoveToSuperview:(UIView *)newSuperview{

    [super willMoveToSuperview:newSuperview];

    // 如果不是UIScrollView，不做任何事情
    if (newSuperview && ![newSuperview isKindOfClass:[UIScrollView class]]) return;

    // 旧的父控件移除监听
    [self removeObservers];

    if (newSuperview) { // 新的父控件
        // 设置宽度

        //设置视图的宽度为父视图的宽度,设置视图的起始位置为滑动视图的左间距
        self.jm_width = newSuperview.jm_width;
        self.jm_x = -_scrollView.jm_insetL;

        // 记录UIScrollView
        _scrollView = (UIScrollView *)newSuperview;
        // 设置永远支持垂直弹簧效果
        _scrollView.alwaysBounceVertical = YES;
        // 记录UIScrollView最开始的contentInset
        _scrollViewOriginalInset = _scrollView.jm_inset;

        // 添加监听
        [self addObservers];
    }
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];

    if (self.state == JMRefreshStateWillRefresh) {
        // 预防view还没显示出来就调用了beginRefreshing
        self.state = JMRefreshStateRefreshing;
    }
}


//监听滑动视图的视图变化
- (void)addObservers
{
    //监听滑动视图的偏移量，内容尺寸的变化
    NSKeyValueObservingOptions options = NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld;
    [self.scrollView addObserver:self forKeyPath:@"contentOffset" options:options context:nil];
    [self.scrollView addObserver:self forKeyPath:@"contentSize" options:options context:nil];
    //设置视图的平移手势为滑动视图的平移手势
    self.pan = self.scrollView.panGestureRecognizer;
    //监听手势的状态
    [self.pan addObserver:self forKeyPath:@"state" options:options context:nil];


}

- (void)removeObservers
{
    [self.superview removeObserver:self forKeyPath:@"contentOffset"];
    [self.superview removeObserver:self forKeyPath:@"contentSize"];
    [self.pan removeObserver:self forKeyPath:@"state"];
    self.pan = nil;

}

//kvo观察到属性变化
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    // 遇到这些情况就直接返回
    if (!self.userInteractionEnabled) return;

    // 这个就算看不见也需要处理
    if ([keyPath isEqualToString:@"contentSize"]) {
        [self scrollViewContentSizeDidChange:change];
    }

    // 看不见
    if (self.hidden) return;
    if ([keyPath isEqualToString:@"contentOffset"]) {
        [self scrollViewContentOffsetDidChange:change];
    } else if ([keyPath isEqualToString:@"state"]) {
        [self scrollViewPanStateDidChange:change];
    }
}

- (void)scrollViewContentOffsetDidChange:(NSDictionary *)change{}
- (void)scrollViewContentSizeDidChange:(NSDictionary *)change{}
- (void)scrollViewPanStateDidChange:(NSDictionary *)change{}

- (void)setRefreshingTarget:(id)target refreshingAction:(SEL)action
{
    self.refreshingTarget = target;
    self.refreshingAction = action;
}

- (void)setState:(JMRefreshState)state
{
    _state = state;

    // 加入主队列的目的是等setState:方法调用完毕、设置完文字后再去布局子控件
    JMRefreshDispatchAsyncOnMainQueue([self setNeedsLayout];);

}

#pragma mark 进入刷新状态
- (void)beginRefreshing
{
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 1.0;
    }];
    self.pullingPercent = 1.0;
    // 只要正在刷新，就完全显示
    if (self.window) {
        self.state = JMRefreshStateRefreshing;
    } else {
        // 预防正在刷新中时，调用本方法使得header inset回置失败
        if (self.state != JMRefreshStateRefreshing) {
            self.state = JMRefreshStateWillRefresh;
            // 刷新(预防从另一个控制器回到这个控制器的情况，回来要重新刷新一下)
            [self setNeedsDisplay];
        }
    }
}

- (void)beginRefreshingWithCompletionBlock:(void (^)(void))completionBlock
{
    self.beginRefreshingCompletionBlock = completionBlock;

    [self beginRefreshing];
}

#pragma mark 结束刷新状态
- (void)endRefreshing
{
    JMRefreshDispatchAsyncOnMainQueue(self.state = JMRefreshStateIdle;)
}

- (void)endRefreshingWithCompletionBlock:(void (^)(void))completionBlock
{
    self.endRefreshingCompletionBlock = completionBlock;

    [self endRefreshing];
}

#pragma mark 是否正在刷新
- (BOOL)isRefreshing
{
    return self.state == JMRefreshStateRefreshing || self.state == JMRefreshStateWillRefresh;
}

- (BOOL)isAutoChangeAlpha
{
    return self.isAutomaticallyChangeAlpha;
}

- (void)setAutomaticallyChangeAlpha:(BOOL)automaticallyChangeAlpha
{
    _automaticallyChangeAlpha = automaticallyChangeAlpha;

    if (self.isRefreshing) return;

    if (automaticallyChangeAlpha) {
        self.alpha = self.pullingPercent;
    } else {
        self.alpha = 1.0;
    }
}


#pragma mark 根据拖拽进度设置透明度
- (void)setPullingPercent:(CGFloat)pullingPercent
{
    _pullingPercent = pullingPercent;

    if (self.isRefreshing) return;

    if (self.isAutomaticallyChangeAlpha) {
        self.alpha = pullingPercent;
    }
}

- (void)executeRefreshingCallback
{
    JMRefreshDispatchAsyncOnMainQueue({
        if (self.refreshingBlock) {
            self.refreshingBlock();
        }
        if ([self.refreshingTarget respondsToSelector:self.refreshingAction]) {
            JMRefreshMsgSend(JMRefreshMsgTarget(self.refreshingTarget), self.refreshingAction, self);
        }
        if (self.beginRefreshingCompletionBlock) {
            self.beginRefreshingCompletionBlock();
        }
    })
}




@end
