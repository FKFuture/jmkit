//
//  UIScrollView+Refresh.m
//  Test
//
//  Created by JM on 2019/10/16.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "UIScrollView+Refresh.h"

#import <objc/runtime.h>

@implementation UIScrollView (Refresh)

#pragma mark - header
static const char MJRefreshHeaderKey = '\0';

//通常在一个类中用@property声明属性，编译器会自动帮我们生成_成员变量、setter和getter方法。类别不能添加成员变量，是因为在分类中是不会自动生成_成员变量、setter和getter方法的，如果仍要添加一个成员变量，编译能成功，但是运行时会崩溃。解决办法就是手动添加setter和getter方法或者通过runtime中objc_getAssociatedObject / objc_setAssociatedObject来访问和生成关联对象。


- (void)setJm_header:(JMRefreshHeader *)jm_header{

    if (jm_header != self.jm_header) {

        [self.jm_header removeFromSuperview];
        [self insertSubview:jm_header atIndex:0];

        //存储新的
        objc_setAssociatedObject(self, &MJRefreshHeaderKey,
                                 jm_header, OBJC_ASSOCIATION_RETAIN);

    }
}

- (JMRefreshHeader *)jm_header{

    return objc_getAssociatedObject(self, &MJRefreshHeaderKey);
}



#pragma mark - footer
static const char MJRefreshFooterKey = '\0';

- (void)setJm_footer:(JMRefreshFooter *)jm_footer{

    if (jm_footer != self.jm_footer) {

        [self.jm_footer removeFromSuperview];
        [self insertSubview:jm_footer atIndex:0];

        // 存储新的
        objc_setAssociatedObject(self, &MJRefreshFooterKey,
                                 jm_footer, OBJC_ASSOCIATION_RETAIN);

    }
}

- (JMRefreshFooter *)jm_footer{

    return objc_getAssociatedObject(self, &MJRefreshFooterKey);
}


#pragma mark - other
- (NSInteger)mj_totalDataCount
{
    NSInteger totalCount = 0;
    if ([self isKindOfClass:[UITableView class]]) {
        UITableView *tableView = (UITableView *)self;

        for (NSInteger section = 0; section < tableView.numberOfSections; section++) {
            totalCount += [tableView numberOfRowsInSection:section];
        }
    } else if ([self isKindOfClass:[UICollectionView class]]) {
        UICollectionView *collectionView = (UICollectionView *)self;

        for (NSInteger section = 0; section < collectionView.numberOfSections; section++) {
            totalCount += [collectionView numberOfItemsInSection:section];
        }
    }
    return totalCount;
}





@end
