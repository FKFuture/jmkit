//
//  JMRefreshFooter.h
//  JMKitUpdate
//
//  Created by JM on 2019/10/21.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "JMBaseRefreshView.h"

NS_ASSUME_NONNULL_BEGIN

@interface JMRefreshFooter : JMBaseRefreshView

@end

NS_ASSUME_NONNULL_END
