//
//  constant.h
//  JMKitUpdate
//
//  Created by JM on 2019/10/21.
//  Copyright © 2019 YQ. All rights reserved.
//

#ifndef constant_h
#define constant_h

#import <objc/message.h>

// 弱引用
#define JMWeakSelf __weak typeof(self) weakSelf = self;

// 日志输出
#ifdef DEBUG
#define JMRefreshLog(...) NSLog(__VA_ARGS__)
#else
#define JMRefreshLog(...)
#endif

// 过期提醒
#define JMRefreshDeprecated(instead) NS_DEPRECATED(2_0, 2_0, 2_0, 2_0, instead)

// 运行时objc_msgSend
#define JMRefreshMsgSend(...) ((void (*)(void *, SEL, UIView *))objc_msgSend)(__VA_ARGS__)
#define JMRefreshMsgTarget(target) (__bridge void *)(target)


// 异步主线程执行，不强持有Self
#define JMRefreshDispatchAsyncOnMainQueue(x) \
__weak typeof(self) weakSelf = self; \
dispatch_async(dispatch_get_main_queue(), ^{ \
typeof(weakSelf) self = weakSelf; \
{x} \
});



#endif /* constant_h */
