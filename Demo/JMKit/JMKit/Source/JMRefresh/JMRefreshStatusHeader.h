//
//  JMRefreshStatusHeader.h
//  JMKitUpdate
//
//  Created by JM on 2019/10/22.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "JMRefreshHeader.h"

NS_ASSUME_NONNULL_BEGIN

@interface JMRefreshStatusHeader : JMRefreshHeader

#pragma mark - 刷新时间相关
/** 利用这个block来决定显示的更新时间文字 */
@property (copy, nonatomic) NSString *(^lastUpdatedTimeText)(NSDate *lastUpdatedTime);
/** 显示上一次刷新时间的label */
@property (weak, nonatomic, readonly) UILabel *lastUpdatedTimeLabel;

#pragma mark - 状态相关
/** 文字距离圈圈、箭头的距离 */
@property (assign, nonatomic) CGFloat labelLeftInset;
/** 显示刷新状态的label */
@property (weak, nonatomic, readonly) UILabel *stateLabel;
/** 设置state状态下的文字 */
- (void)setTitle:(NSString *)title forState:(JMRefreshState)state;

@end

NS_ASSUME_NONNULL_END
