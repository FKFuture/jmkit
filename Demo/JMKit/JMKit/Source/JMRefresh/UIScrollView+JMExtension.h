//
//  UIScrollView+JMExtension.h
//  JMKitUpdate
//
//  Created by JM on 2019/10/21.
//  Copyright © 2019 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIScrollView (JMExtension)

@property (readonly, nonatomic) UIEdgeInsets jm_inset;

@property (assign, nonatomic) CGFloat jm_insetT;
@property (assign, nonatomic) CGFloat jm_insetB;
@property (assign, nonatomic) CGFloat jm_insetL;
@property (assign, nonatomic) CGFloat jm_insetR;

@property (assign, nonatomic) CGFloat jm_offsetX;
@property (assign, nonatomic) CGFloat jm_offsetY;

@property (assign, nonatomic) CGFloat jm_contentW;
@property (assign, nonatomic) CGFloat jm_contentH;


@end

