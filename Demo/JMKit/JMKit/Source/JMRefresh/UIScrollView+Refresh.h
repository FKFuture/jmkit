//
//  UIScrollView+Refresh.h
//  Test
//
//  Created by JM on 2019/10/16.
//  Copyright © 2019 YQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JMRefreshHeader.h"
#import "JMRefreshFooter.h"

NS_ASSUME_NONNULL_BEGIN

@interface UIScrollView (Refresh)

/** 下拉刷新控件 */
@property (strong, nonatomic)JMRefreshHeader  *jm_header;
/** 上拉刷新控件 */
@property (strong, nonatomic) JMRefreshFooter *jm_footer;

#pragma mark - other
- (NSInteger)mj_totalDataCount;



@end

NS_ASSUME_NONNULL_END
