//
//  JMRefreshAutoHeader.h
//  JMKitUpdate
//
//  Created by JM on 2019/10/22.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "JMRefreshStatusHeader.h"

NS_ASSUME_NONNULL_BEGIN

@interface JMRefreshAutoHeader : JMRefreshStatusHeader

@property (weak, nonatomic, readonly) UIImageView *arrowView;
/** 菊花的样式 */
@property (assign, nonatomic) UIActivityIndicatorViewStyle activityIndicatorViewStyle;


@end

NS_ASSUME_NONNULL_END
