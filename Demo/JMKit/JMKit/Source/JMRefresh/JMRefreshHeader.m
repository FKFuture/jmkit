//
//  JMRefreshHeader.m
//  JMKitUpdate
//
//  Created by JM on 2019/10/21.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "JMRefreshHeader.h"
#import "UIScrollView+JMExtension.h"
#import "UIView+JMExtension.h"
#import "constant.h"

@interface JMRefreshHeader ()

@property (assign, nonatomic) CGFloat insetTDelta;


@end

@implementation JMRefreshHeader

+(instancetype)headerWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock{

    JMRefreshHeader *Header = [[self alloc] init];
    Header.refreshingBlock = refreshingBlock;

    return Header;

}

+(instancetype)headerWithRefreshingTarget:(id)target refreshingAction:(SEL)action{

    JMRefreshHeader *Header = [[self alloc] init];
    [Header setRefreshingTarget:target refreshingAction:action];

    return Header;

}

//重写父类的方法
- (void)prepare{

    [super prepare];

    //设置默认高度
    self.lastUpdatedTimeKey = @"MJRefreshHeaderLastUpdatedTimeKey";
//    self.height = 54.0;
    self.jm_height = 100.0;



}

- (void)placeSubviews{

    [super placeSubviews];

    // 设置y值(当自己的高度发生改变了，肯定要重新调整Y值，所以放到placeSubviews方法中设置y值)
    self.jm_y = - self.jm_height - self.ignoredScrollViewContentInsetTop;

}

- (void)willMoveToWindow:(UIWindow *)newWindow{

    if (!newWindow && self.isRefreshing) {
        [self endRefreshing];
    }

}


#pragma mark 结束刷新状态
- (void)endRefreshing
{

    JMRefreshDispatchAsyncOnMainQueue(self.state = JMRefreshStateIdle;)

}

//滑动视图内容偏移量改变
- (void)scrollViewContentOffsetDidChange:(NSDictionary *)change
{
    [super scrollViewContentOffsetDidChange:change];

    // 在刷新的refreshing状态
    if (self.state == JMRefreshStateRefreshing) {
        // 暂时保留
        if (self.window == nil) return;

        // sectionheader停留解决
        CGFloat insetT = - self.scrollView.jm_offsetY > _scrollViewOriginalInset.top ? - self.scrollView.jm_offsetY : _scrollViewOriginalInset.top;
        insetT = insetT > self.jm_height + _scrollViewOriginalInset.top ? self.jm_height + _scrollViewOriginalInset.top : insetT;
        self.scrollView.jm_insetT = insetT;

        self.insetTDelta = _scrollViewOriginalInset.top - insetT;
        return;
    }

    // 跳转到下一个控制器时，contentInset可能会变
    _scrollViewOriginalInset = self.scrollView.jm_inset;

    // 当前的contentOffset
    CGFloat offsetY = self.scrollView.jm_offsetY;
    // 头部控件刚好出现的offsetY
    CGFloat happenOffsetY = - self.scrollViewOriginalInset.top;

    // 如果是向上滚动到看不见头部控件，直接返回
    // >= -> >
    if (offsetY > happenOffsetY) return;

    // 普通 和 即将刷新 的临界点
    CGFloat normal2pullingOffsetY = happenOffsetY - self.jm_height;
    CGFloat pullingPercent = (happenOffsetY - offsetY) / self.jm_height;

    if (self.scrollView.isDragging) { // 如果正在拖拽
        self.pullingPercent = pullingPercent;
        if (self.state == JMRefreshStateIdle && offsetY < normal2pullingOffsetY) {
            // 转为即将刷新状态
            self.state = JMRefreshStatePulling;
        } else if (self.state == JMRefreshStatePulling && offsetY >= normal2pullingOffsetY) {
            // 转为普通状态
            self.state = JMRefreshStateIdle;
        }


    } else if (self.state == JMRefreshStatePulling) {// 即将刷新 && 手松开
        // 开始刷新
        [self beginRefreshing];
    } else if (pullingPercent < 1) {
        self.pullingPercent = pullingPercent;
    }
}

- (void)setState:(JMRefreshState)state
{

    JMRefreshState oldState = self.state;
    if (state == oldState) return;
    [super setState:state];


    // 根据状态做事情
    if (state == JMRefreshStateIdle) {
        if (oldState != JMRefreshStateRefreshing) return;

        // 保存刷新时间
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:self.lastUpdatedTimeKey];
        [[NSUserDefaults standardUserDefaults] synchronize];

        // 恢复inset和offset
        [UIView animateWithDuration:0.4 animations:^{
            self.scrollView.jm_insetT += self.insetTDelta;

            // 自动调整透明度
            if (self.isAutomaticallyChangeAlpha) self.alpha = 0.0;
        } completion:^(BOOL finished) {
            self.pullingPercent = 0.0;

            if (self.endRefreshingCompletionBlock) {
                self.endRefreshingCompletionBlock();
            }
        }];
    } else if (state == JMRefreshStateRefreshing) {
        JMRefreshDispatchAsyncOnMainQueue({
            [UIView animateWithDuration:0.25 animations:^{
                if (self.scrollView.panGestureRecognizer.state != UIGestureRecognizerStateCancelled) {
                    NSLog(@"来了大爷");
                    CGFloat top = self.scrollViewOriginalInset.top + self.jm_height;
                    // 增加滚动区域top
                    self.scrollView.jm_insetT = top;
                    // 设置滚动位置
                    CGPoint offset = self.scrollView.contentOffset;
                    offset.y = -top;
                    [self.scrollView setContentOffset:offset animated:NO];
                }
            } completion:^(BOOL finished) {
                [self executeRefreshingCallback];
            }];
        })
    }
}

#pragma mark - 公共方法
- (NSDate *)lastUpdatedTime
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:self.lastUpdatedTimeKey];
}

- (void)setIgnoredScrollViewContentInsetTop:(CGFloat)ignoredScrollViewContentInsetTop {
    _ignoredScrollViewContentInsetTop = ignoredScrollViewContentInsetTop;

    self.jm_y = - self.jm_height - _ignoredScrollViewContentInsetTop;
}








@end
