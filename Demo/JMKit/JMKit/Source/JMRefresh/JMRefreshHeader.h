//
//  JMRefreshHeader.h
//  JMKitUpdate
//
//  Created by JM on 2019/10/21.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "JMBaseRefreshView.h"

NS_ASSUME_NONNULL_BEGIN

@interface JMRefreshHeader : JMBaseRefreshView

//初始化header
+ (instancetype)headerWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock;

+ (instancetype)headerWithRefreshingTarget:(id)target refreshingAction:(SEL)action;


/** 这个key用来存储上一次下拉刷新成功的时间 */
@property (copy, nonatomic) NSString *lastUpdatedTimeKey;
/** 上一次下拉刷新成功的时间 */
@property (strong, nonatomic, readonly) NSDate *lastUpdatedTime;

/** 忽略多少scrollView的contentInset的top */
@property (assign, nonatomic) CGFloat ignoredScrollViewContentInsetTop;





@end

NS_ASSUME_NONNULL_END
