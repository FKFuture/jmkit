//
//  UIScrollView+JMExtension.m
//  JMKitUpdate
//
//  Created by JM on 2019/10/21.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "UIScrollView+JMExtension.h"
#import <objc/runtime.h>
#import <Availability.h>

////忽略警告
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunguarded-availability-new"



@implementation UIScrollView (JMExtension)

static BOOL JMrespondsToAdjustedContentInset_;

//instancesRespondToSelector是一个类方法,用来判断该类的的实例(即对象)是否响应某个方法
//respondsToSelector 是一个实例(对象)方法,用来判断该实例(对象)是否响应某个方法

@dynamic jm_contentH;
@dynamic jm_contentW;
@dynamic jm_insetR;
@dynamic jm_insetL;
@dynamic jm_insetB;
@dynamic jm_insetT;
@dynamic jm_offsetX;
@dynamic jm_offsetY;




+ (void)initialize
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        JMrespondsToAdjustedContentInset_ = [self instancesRespondToSelector:@selector(adjustedContentInset)];
    });
}

- (UIEdgeInsets)jm_inset{


#ifdef __IPHONE_11_0
    if (JMrespondsToAdjustedContentInset_) {
        return self.adjustedContentInset;
    }


#endif
    return self.contentInset;

}

//设置滑动视图的顶部内边距
- (void)setJm_insetT:(CGFloat)jm_insetT
{
    UIEdgeInsets inset = self.contentInset;
    inset.top = jm_insetT;
#ifdef __IPHONE_11_0
    if (JMrespondsToAdjustedContentInset_) {
        inset.top -= (self.adjustedContentInset.top - self.contentInset.top);
    }
#endif
    self.contentInset = inset;
}

- (CGFloat)jm_insetT
{
    return self.jm_inset.top;
}

//设置滑动视图的底部内间距
- (void)setJM_insetB:(CGFloat)jm_insetB
{
    UIEdgeInsets inset = self.contentInset;
    inset.bottom = jm_insetB;
#ifdef __IPHONE_11_0
    if (JMrespondsToAdjustedContentInset_) {
        inset.bottom -= (self.adjustedContentInset.bottom - self.contentInset.bottom);
    }
#endif
    self.contentInset = inset;
}

- (CGFloat)jm_insetB
{
    return self.jm_inset.bottom;
}

//设置滑动视图的左边间距
- (void)setJM_insetL:(CGFloat)jm_insetL
{
    UIEdgeInsets inset = self.contentInset;
    inset.left = jm_insetL;
#ifdef __IPHONE_11_0
    if (JMrespondsToAdjustedContentInset_) {
        inset.left -= (self.adjustedContentInset.left - self.contentInset.left);
    }
#endif
    self.contentInset = inset;
}

- (CGFloat)jm_insetL
{
    return self.jm_inset.left;
}

//设置滑动视图的右边间距
- (void)setJM_insetR:(CGFloat)jm_insetR
{
    UIEdgeInsets inset = self.contentInset;
    inset.right = jm_insetR;
#ifdef __IPHONE_11_0
    if (JMrespondsToAdjustedContentInset_) {
        inset.right -= (self.adjustedContentInset.right - self.contentInset.right);
    }
#endif
    self.contentInset = inset;
}

- (CGFloat)jm_insetR
{
    return self.jm_inset.right;
}

//设置滑动视图的x偏移量
- (void)setJM_offsetX:(CGFloat)jm_offsetX
{
    CGPoint offset = self.contentOffset;
    offset.x = jm_offsetX;
    self.contentOffset = offset;
}

- (CGFloat)jm_offsetX
{
    return self.contentOffset.x;
}

//设置滑动视图的右边偏移量
- (void)setJM_offsetY:(CGFloat)jm_offsetY
{
    CGPoint offset = self.contentOffset;
    offset.y = jm_offsetY;
    self.contentOffset = offset;
}

- (CGFloat)jm_offsetY
{
    return self.contentOffset.y;
}

//设置滑动视图的内容宽度
- (void)setJM_contentW:(CGFloat)jm_contentW
{
    CGSize size = self.contentSize;
    size.width = jm_contentW;
    self.contentSize = size;
}

- (CGFloat)jm_contentW
{
    return self.contentSize.width;
}

//设置滑动视图的内容高度
- (void)setJM_contentH:(CGFloat)jm_contentH
{
    CGSize size = self.contentSize;
    size.height = jm_contentH;
    self.contentSize = size;
}

- (CGFloat)jm_contentH{

    return self.contentSize.height;
}


@end
