//
//  JMAnimationHeader.m
//  JMKitUpdate
//
//  Created by JM on 2019/10/23.
//  Copyright © 2019 YQ. All rights reserved.
//

#import "JMAnimationHeader.h"
#import "UIView+JMExtension.h"

@implementation JMAnimationHeader{


    __unsafe_unretained UILabel *_animationLabel;
    __unsafe_unretained UIImageView *_statusImageView;


}

- (UILabel *)animationLabel{

    if (!_animationLabel) {

            UILabel *label = [[UILabel alloc] init];
            label.font = [UIFont boldSystemFontOfSize:14];
            label.textColor = [UIColor redColor];
            label.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            label.textAlignment = NSTextAlignmentCenter;
            label.backgroundColor = [UIColor clearColor];
           _animationLabel = label;
           [self addSubview:label];

    }

    return _animationLabel;

}

- (UIImageView *)statusImageView{

    if (!_statusImageView) {

        UIImageView *image = [[UIImageView alloc] init];
        _statusImageView = image;
        [self addSubview:image];

    }

    return _statusImageView;

}



//设置最后刷新时间
- (void)setLastUpdatedTimeKey:(NSString *)lastUpdatedTimeKey
{
    [super setLastUpdatedTimeKey:lastUpdatedTimeKey];

    // 如果label隐藏了，就不用再处理
//    if (self.lastUpdatedTimeLabel.hidden) return;

    NSDate *lastUpdatedTime = [[NSUserDefaults standardUserDefaults] objectForKey:lastUpdatedTimeKey];

    // 如果有block
//    if (self.lastUpdatedTimeText) {
//        self.lastUpdatedTimeLabel.text = self.lastUpdatedTimeText(lastUpdatedTime);
//        return;
//    }

    if (lastUpdatedTime) {
        // 1.获得年月日
        NSCalendar *calendar = [self currentCalendar];
        NSUInteger unitFlags = NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay |NSCalendarUnitHour |NSCalendarUnitMinute;
        NSDateComponents *cmp1 = [calendar components:unitFlags fromDate:lastUpdatedTime];
        NSDateComponents *cmp2 = [calendar components:unitFlags fromDate:[NSDate date]];

        // 2.格式化日期
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        BOOL isToday = NO;
        if ([cmp1 day] == [cmp2 day]) { // 今天
            formatter.dateFormat = @" HH:mm";
            isToday = YES;
        } else if ([cmp1 year] == [cmp2 year]) { // 今年
            formatter.dateFormat = @"MM-dd HH:mm";
        } else {
            formatter.dateFormat = @"yyyy-MM-dd HH:mm";
        }
        NSString *time = [formatter stringFromDate:lastUpdatedTime];

        NSString *lastUpdateString=    [NSString stringWithFormat:@"%@%@%@",
                                              @"第一次刷新",
                                              isToday ? @"今天操作" : @"",
                                              time];
        self.animationLabel.text = lastUpdateString;


        // 3.显示日期
    } else {

        NSString *lastUpdateString= [NSString stringWithFormat:@"%@%@",
                                     @"上次刷新时间",
                                     @"最后一次刷新时间"];
        self.animationLabel.text = lastUpdateString;


    }
}

- (NSCalendar *)currentCalendar {
    if ([NSCalendar respondsToSelector:@selector(calendarWithIdentifier:)]) {
        return [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    }
    return [NSCalendar currentCalendar];
}

#pragma mark - 覆盖父类的方法
- (void)prepare
{
    [super prepare];

    self.jm_height = 100;

    // 初始化文字
    [self setTitle:@"闲置状态刷新完毕" forState:JMRefreshStateIdle];
    [self setTitle:@"下拉看到更多小哥哥哦" forState:JMRefreshStatePulling];
    [self setTitle:@"正在刷新中" forState:JMRefreshStateRefreshing];

}

- (void)setTitle:(NSString *)title forState:(JMRefreshState)state
{
    if (title == nil) return;

    //根据不同的状态做不同的处理
    if (state == JMRefreshStateIdle) {
        NSLog(@"又回到了最初的位置");
    }else if (state == JMRefreshStatePulling){
        NSLog(@"快松手");
    }else if (state == JMRefreshStateRefreshing){
        NSLog(@"火速加载中");
    }

    self.animationLabel.text = title;
}



- (void)placeSubviews
{
    [super placeSubviews];

    // 箭头的中心点

    if (!self.animationLabel.hidden) {

        self.animationLabel.frame = CGRectMake(self.jm_width *0.5, self.jm_height *0.5, 200, 30);
        self.animationLabel.center = CGPointMake(self.jm_width *0.5, self.jm_height *0.5);

    }

    if (!_statusImageView.hidden) {

        self.statusImageView.bounds = CGRectMake(0, 0, 30, 30);
        self.statusImageView.center = CGPointMake(10, 20);
        self.statusImageView.image = [UIImage imageNamed:@"icon_myworld"];

    }
}


- (void)setState:(JMRefreshState)state
{


    JMRefreshState oldState = self.state; \
    if (state == oldState) return; \
    [super setState:state];
    // 重新设置key（重新显示时间）
    self.lastUpdatedTimeKey = self.lastUpdatedTimeKey;

    // 根据状态做事情
    if (state == JMRefreshStateIdle) {
        self.animationLabel.text = @"又回到了最初的位置";
        if (oldState == JMRefreshStateRefreshing) {
            //恢复原状
            self.statusImageView.transform = CGAffineTransformIdentity;
            [UIView animateWithDuration:0.4 animations:^{

            } completion:^(BOOL finished) {
                // 如果执行完动画发现不是idle状态，就直接返回，进入其他状态
                if (self.state != JMRefreshStateIdle) return;

            }];
        } else {

            [UIView animateWithDuration:0.25 animations:^{
                self.statusImageView.transform = CGAffineTransformIdentity;
            }];
        }
    } else if (state == JMRefreshStatePulling) {

        self.animationLabel.text = @"快松手";

    } else if (state == JMRefreshStateRefreshing) {

        [UIView animateWithDuration:0.25 animations:^{

            CGFloat width = self.jm_width/2 - 15 - 10;
            self.statusImageView.transform = CGAffineTransformMakeTranslation(width, 0);

        }];

        //设置上下跳动
        CABasicAnimation *basic = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        basic.duration = 1;
        basic.fromValue = @0.5;
        basic.toValue = @2;
        basic.repeatCount =2;
        basic.autoreverses = YES;
        //这两行代码可以不删除动画效果，保持在动画结束后。
        //    basic.removedOnCompletion = NO;
        //    basic.fillMode = kCAFillModeForwards;
        [self.statusImageView.layer addAnimation:basic forKey:nil];


        self.animationLabel.text = @"火速加载中...";


    }
}
















@end
